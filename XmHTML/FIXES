This file: FIXES.
Describes the list of bugfixes made.

Bugfix numbering: <mm/dd/yy>-<bugfix number>, <source file>, <initials>

initials:
	kdh:	Koen D'Hondt (alias newt), ripley@xs4all.nl
	dp:		Dick Porter, dick@cymru.net
	tma:	Thanh Ma, tma@encore.com
	rr:		Ricky Ralston (rlr@wscoe5.atl.hp.com)
	ro:		Richard Offer (offer@sgi.com)
	shl:	Susan Liebeskind (shl@cc.gatech.edu)
	aj:		Antony Jankelowitz <antony@medsim.com>
	dbl:	David B. Lewis <dbl@ics.com>

------------------
02/12/98-01, parse.c, kdh
	Trailing contents on closing elements is now properly ignored.

------------------
01/04/98-01, format.c, kdh
	Documents containing only plain text and without a missing BODY
	tag are now rendered.

------------------
15/12/97, forms.c, ro
	HTML Form widgets are no longer actively destroyed when the parent
	HTML widget is destroyed. Previously, this lead to an XError.

15/12/97-02, layout.c, kdh
	If a line is ended by an explicit linebreak, any leading space for the
	first word on the next line is removed. As a result of this, the text
	justification routines don't shift the word (appearing immediatly after
	a linebreak) to the left.

------------------
12/14/97-01, format.c, kdh
	A dummy element is now inserted at the list of objects to be rendered.
	This ensures that the first true element to be rendered gets rendered
	properly.

------------------
11/12/97-01, format.c, kdh
	reuse old XmHTMLObjectTableElement if it was ignored on the previous
	pass instead of freeing it and then allocating a new one.

------------------
11/11/97-01, XmHTML.c, dbl
	ScrollToLine bugfix, correct line number is now selected in a block
	of words.

------------------
10/27/97-01, XmHTMLfuncs.h, plc.h, shl
	SparcWorks cpp bug workaround

------------------
10/26/97-01, XmHTML.c, kdh
	save work_width when the widget is mapped to screen for the first time.

	Background:

	When a XmHTML Widget is created, XmHTML's Initialize method is called.
	This method initializes the internal data structures and creates XmHTML's
	subwidgets, but it does not create a Window (which get's done by the
	widget's Realize method). Anywhere between the return from the widget's
	Initialize method and the time it gets the MapNotify event, the size of
	the widget can change. This poses a problem as the Widget's layout
	algorithms (and it's Resize method) don't do anything *until* the Widget
	has a window (and thus it's final size is known). To make things even
	more complicated, a XmHTML Widget inherits its realize method from it's 
	superclass (meaning we don't have any control over this).

	To get around all this, a XmHTML Widget uses a SubstructureNotify event
	handler get the first MapNotify event. Once this event is received, the
	*current* size of the work area is computed and any required GC's are
	created. From that point on the Widget can work as expected (the
	SubstructureNotify event handler is removed as it's no longer needed).

------------------
10/22/97-01, callbacks.c, XmHTML.c, kdh
	ignore vertical scroll requests when the document doesn't require
	a vertical scrollbar. Previously scrolling was *always* done which
	rendered text invisible when XmHTML jumped to an anchor at the bottom
	of a page.

------------------
09/17/97-01, format.c, kdh
	explicitly reset list of named anchors. Not resetting this caused
	a core in the following sequence of events:
	1. load a document;
	2. load an *empty* document (--> list was cleared but not reset);
	3. load a new document (--> cored, previous list is invalid);

09/17/97-02, XmHTML.c, kdh
	always free list of imagemaps when document layout recomputation is
	required;

------------------
09/01/97-01, images.c, rr
	added a XmIsHTML() in _XmHTMLFreeImageInfo. Required 'cause it's also
	called from the XmImage code, where the parent isn't necesseraly a
	XmHTML widget.

09/01/97-02, images.c, kdh
	memory leak fix in getImageAttributes. The alt text was allocated twice
	for <IMG> specs without an ALT attribute.

09/01/97-03, XCC.c, kdh
	memory leak fix in XCCFree(). HashTable wasn't freed.

------------------
08/30/97-01, quantize.c, rr
	array bounds error fix.

------------------
08/29/97-01, XCC.c, rr
	color allocation verification array initialization.

------------------
08/04/97-01, parse.c, kdh
	element verification: elements with an optional closure are now being
	flushed when they prevent a new element from becoming legal. Previously
	the new element was discarded, which sometimes lead to bad results.
	(e.i., in <table><tr><td> ...</table> the closing table was discarded and
	thus everything following was placed inside the table.)

------------------
07/24/97, format.c, kdh
	No horizontal spacing for images when they are part of <pre></pre>.

------------------
07/09/97-01, format.c, kdh
	memory leak in anchor freeing: the name field was never freed.

------------------
07/04/97-01, paint.c, kdh
	Fixed baseline adjustment for form components: text aligned in the middle
	but baseline is fully shifted down now.

------------------
07/03/97-01, paint.c, kdh
	Fix in text outlining: a space is now also added before the longest word
	in a line instead of just shifting all following words to the right. This
	produces *much* better results.

07/03/97-02, paint.c, kdh
	Special case for outlining of a line consisting of two words: the second
	word is now just flushed to the right margin. The outlining algorithm 
	can't handle this case properly, it always breaks out after the maximum
	allowable iterations.

07/03/97-03, paint.c, kdh
	Fixed a bug in baseline adjustment: when the baseline changes while
	laying out a line, the baseline of the words already placed on the line
	wasn't updated at all.

07/03/97-04, paint.c, kdh
	image and form text object baseline checking messed up when the last line
	in a paragraph was being laid out. The conditional used for lineheight 
	updating always evaluated to True for these cases, while it should have
	been evaluated to False when it was the last object in a paragraph.

------------------
06/18/97-01, parse.c, dp
	Fix in parser driver: previously it was assumed that if the document had
	undergone more than 1 pass, the previous text could be freed without
	problems.

06/18/97-02, paint.c, kdh
	Fixed incorrect handling of transparent animation frames in animations
	with a dispose by background disposal method. Previously the clipmask
	was also used when substituting the current background setting. Pointed out
	by dp.

------------------
06/17/97-01, XmHTML.c, aj
	An explicit check for the presence of the XmNvalue resource is now
	made to see if new text has indeed been set. Previously this test was
	not done which in certain cases could cause the present contents to be
	lost when a SetValues call was made without setting new text.

------------------
06/16/97, colors.c, kdh
	Changed XmHTMLAllocColor to not use an XCC, the xcc with which a color
	was allocated most likely isn't going to be around when the color is
	freed using XmHTMLFreeColor (and it was messing with XmHTML's private
	color allocation as well).

06/16/97-01, XCC.c, kdh
	Fixed incorrect RGB shifts in XCCGetPixels. Previously all components
	were shifted out of range, causing the mapping from unallocated to
	allocated colors to fail miserably.

------------------
05/26/97-01, paint.c, kdh
	Fixed memory leak/serious bug in DrawAnchor: words was freed when
	all_words should have been freed. This obviously caused a crash since
	this was only done when the test if(words == NULL) was satisfied...
	It was probably very late when I added that one...

------------------
04/30/97-1, parse.c, shl
	Fix to recognize comments with a dashcount that is not a multiple of four.

------------------
04/27/97-01, XmHTML.c, kdh
	Horizontal scrollbar check did not take the presence of a possible vertical
	scrollbar into account, which could lead to the loss of text.

------------------
04/26/97-01, paint.c, kdh
	Line numbers did not get set on preformatted text.

------------------
04/23/97-01, XCC.c, ro
	SGI fix: return value from XQueryColors was returned while XQueryColors
	is declared to return void...

------------------
04/03/97-01, paint.c, kdh
	anchor rendering when XmNanchorButtons was False and a body image present
	would draw anchors against the current background color instead of
	being overlaid on the current background.

------------------
04/01/97-01, images.c, kdh
	used image data instead of image clip data to scale the clipmask for
	an image.

------------------
03/25/97-01, paint.c, kdh 
	image anchors did not always painted themselves correctly: the image
	position was first compared with an exposure region before painting the
	anchor rectangle. Reversing these two actions corrected the problem.
03/25/97-02, images.c, kdh
	changed all XtDisplay(html->html.work_area) to XtDisplay((Widget)html).
	The Destroy method caused a segv when it was freeing the images: the
	work_area was already destroyed and hence didn't have a display anymore.

------------------
03/24/97-01, readJPEG.c, rr
	allocation of image buffer was done using width*width instead of
	height*width.

------------------
03/23/97-01, format.c, kdh
	increase tab count when not in a word. Caused a buffer overwrite when
	tabs were expanded.

------------------
02/26/97-01, XmHTML.c, kdh
	reset paint_start and paint_end when loading new document.
02/26/97-02, parse.c, dp
	ExpandEscapes: changed from while(True) to while(*string)

------------------
02/21/97-01, format.c, kdh
	changed object_type for MAP and AREA elements from OBJ_BLOCK to
	OBJ_NONE: multiple images and maps on a single line are now
	displayed correctly.
/2/21/97-02, paint.c, kdh
	added a SetBlock for OBJ_NONE objects. Before, the x and y positions 
	for the given object were left at 0. This broke the exposed region
	calculation in DrawRedisplay.

------------------
02/05/97-01, format.c, dp
	This one-liner typo fixes a core dump when an anchor has both TARGET 
	and HREF options.

------------------
02/03/07-01, format.c, dp
	use current font if LoadQueryFont fails. Previously a NULL font member
	was passed back which would cause a sigsegv eventually.

02/03/97-02, colors.c, dp
	forgot to reset color stack depth. Worked fine as long as a single
	XmHTML widget was destroyed. Gave a BadAccess when a second widget with
	a different number of colors was being destroyed: some or all pixel values 
	are already de-allocated.

02/03/97-03, StringUtil.c, tma
	Added strncasecmp and strcasecmp for systems that don't have it.
	Add a -DNEEDS_STRCASECMP to CPPFLAGS if your system doesn't have the
	above functions.

02/03/97-04, XmHTML.c, kdh
	added check whether passed anchor name is non-null.

02/03/97-05, format.c, kdh
	href field now always has a value. Unknown/misspelled anchors caused
	a sigsegv when doing strcmp/strcasecmp on a NULL href field.

------------------
01/30/97-01, paint.c, kdh
	added a sanity check: don't do a thing if no words have to be painted.

01/30/97-02, format.c, kdh
	major bug fixed: the piece of code that computes how many bytes is
	required to fully expand raw, preformatted text did underestimate in
	a few occasions. This resulted in a sigsegv when freeing the ObjectTable
	list (and probably a bunch of other things).

01/30/97-03, paint.c, kdh
	maximum text width determination for preformatted text now takes 
	margin_width into account.

01/30/97-04, XmHTML.c, kdh
01/30/97-05, callbacks.c, kdh
	check the value for the vertical scrollbar so it does not exceed the
	maximum allowable scrollbar value.

------------------
01/28/97-01, parse.c, kdh
	memory leak fixed: added a free when no element has been stored.

01/28/98-02, format.c, kdh
01/28/97-03, images.c, kdh 
	memory leak fixed, forgot a free when successfull return from
	_XmHTMLTagGetValue.

01/28/97-04, parse.c, kdh
	CheckTermination, the ObjectType for an inserted </p> was wrongly
	set at HT_DT instead of HT_P. This caused an unbalanced alignment
	stack in _XmHTMLformatObjects. This one was hard to track down.

01/28/97-05, format.c, kdh
	memory leak fixed. 

01/28/97-06, paint.c, kdh 
	added a sanity check: no layout if the document is empty

------------------
01/26/97-01, XmHTML.c, kdh
	XmHTML no longer causes a BadWindow error when text is set using
	XtSetValues upon the widget *before* XtRealizeWidget was called.

------------------
01/25/97-01, paint.c, kdh
	SetBlock now uses the height of the provided font to increase the
	vertical offset instead of the default font. This is necessary for
	text with a different pointsize than the default font: the linespacing
	was too small, causing text to be overlaid.

------------------
01/24/97-01, XmHTML.c, kdh 
	when scrolling to the right, text is now moved beyond the right margin
	instead of disappearing at the right margin (which would be visible
	if the margin is non-zero).

------------------
01/23/97-01, XmHTML.c, kdh
	when scrolling to the left, text that has been rendered into the right
	margin is now properly removed.

01/23/97-02, XmHTML.c, kdh
	value of horizontal scrollbar slider pos is now checked before it's 
	applied.

------------------
01/20/97-01, XmHTML.c, kdh 
	scrollCB now returns when a scrollbar slider hasn't been moved but just
	clicked on.

01/20/97-02, format.c, kdh
	HT_BR no longer breaks the linefeeding mechanism.

01/20/97-03, format.c kdh
	inadvertedly left HT_PRE out NextFont. Caused an unknown font switch
	warning.

------------------
01/17/97-01, parse.c, kdh
	fixes for removing trailing spaces of elements without attributes.

------------------
01/14/97-01, parse.c, kdh
	fix HTML comment parsing: added a break statement.

01/14/97-02, parse.c, kdh
	fix HTML comment parsing: HTML comment dashes only occur in a multiple of 2.

------------------
01/12/97-01, parse.c, kdh 
	added a free(buf) in _XmHTMLGetHorizontalAlignment

01/12/97-01, parse.c, kdh 
	added a free(buf) in _XmHTMLGetVerticalAlignment

------------------
12/21/96-01, parse.c, kdh
	added a free(buf) in _XmHTMLTagCheckValue



#define _XmExtBuildDll_
 

/*****
* Automatically generated file.
*  ***DO NOT EDIT THIS FILE***
*****/
/*****
* mkStrings Version 1.30, Build Date: Aug 10 1998 16:30:15
* File created at: Mon Aug 10 16:30:19 1998
*****/

#include <Xm/XmP.h>
#if XtSpecificationRelease < 6
#include <X11/IntrinsicP.h>
#endif

#ifndef _XmConst
# ifdef __STDC__
#  define _XmConst const
# else
#  define _XmConst
# endif
#endif

/*****
* The missing commas are intentional. Let the compiler
* concatenate the strings
*****/

/*****
* Need declare '_XmHTMLStrings' symbol as export
*****/
 
#include <Xm/HTMLStrings.h>

_XmConst char _XmHTMLStrings[] =
/*     0 */ "\0"
/*     1 */ "AnchorUnderlineType\0"
/*    21 */ "AnchorVisitedProc\0"
/*    39 */ "BackingStore\0"
/*    52 */ "BalloonStyle\0"
/*    65 */ "BorderSize\0"
/*    76 */ "ClientData\0"
/*    87 */ "CornerStyle\0"
/*    99 */ "ConversionMode\0"
/*   114 */ "DecodeGIFProc\0"
/*   128 */ "EnableMode\0"
/*   139 */ "EventProc\0"
/*   149 */ "HTMLWarningMode\0"
/*   165 */ "ImageProc\0"
/*   175 */ "MaxImageColors\0"
/*   190 */ "PerfectColors\0"
/*   204 */ "PopdownDelay\0"
/*   217 */ "PopupDelay\0"
/*   228 */ "ProgressiveEndProc\0"
/*   247 */ "ProgressiveInitialDelay\0"
/*   271 */ "ProgressiveMaximumDelay\0"
/*   295 */ "ProgressiveMinimumDelay\0"
/*   319 */ "ProgressiveReadProc\0"
/*   339 */ "ScreenGamma\0"
/*   351 */ "TopLine\0"
/*   359 */ "Transparent\0"
/*   371 */ "alphaChannelProcessing\0"
/*   394 */ "anchorActivatedBackground\0"
/*   420 */ "anchorActivatedForeground\0"
/*   446 */ "anchorButtons\0"
/*   460 */ "anchorCursor\0"
/*   473 */ "anchorDisplayCursor\0"
/*   493 */ "anchorForeground\0"
/*   510 */ "anchorTargetForeground\0"
/*   533 */ "anchorTargetUnderlineType\0"
/*   559 */ "anchorTrackCallback\0"
/*   579 */ "anchorUnderlineType\0"
/*   599 */ "anchorVisitedForeground\0"
/*   623 */ "anchorVisitedProc\0"
/*   641 */ "anchorVisitedUnderlineType\0"
/*   668 */ "backingStore\0"
/*   681 */ "balloonStyle\0"
/*   694 */ "borderSize\0"
/*   705 */ "bodyImage\0"
/*   715 */ "charset\0"
/*   723 */ "clientData\0"
/*   734 */ "cornerStyle\0"
/*   746 */ "debugDisableWarnings\0"
/*   767 */ "debugEnableFullOutput\0"
/*   789 */ "debugFilePrefix\0"
/*   805 */ "debugLevels\0"
/*   817 */ "debugNoAnimationLoopCount\0"
/*   843 */ "debugSaveClipmasks\0"
/*   862 */ "decodeGIFProc\0"
/*   876 */ "documentCallback\0"
/*   893 */ "enableBadHTMLWarnings\0"
/*   915 */ "enableBodyColors\0"
/*   932 */ "enableBodyImages\0"
/*   949 */ "enableDocumentColors\0"
/*   970 */ "enableDocumentFonts\0"
/*   990 */ "enableFormColors\0"
/*  1007 */ "enableOutlining\0"
/*  1023 */ "eventCallback\0"
/*  1037 */ "eventProc\0"
/*  1047 */ "fontFamily\0"
/*  1058 */ "fontFamilyFixed\0"
/*  1074 */ "fontSizeFixedList\0"
/*  1092 */ "fontSizeList\0"
/*  1105 */ "formCallback\0"
/*  1118 */ "frameCallback\0"
/*  1132 */ "freezeAnimations\0"
/*  1149 */ "imageEnable\0"
/*  1161 */ "imageMapToPalette\0"
/*  1179 */ "imagePalette\0"
/*  1192 */ "imageProc\0"
/*  1202 */ "imageRGBConversion\0"
/*  1221 */ "imagemapBoundingBoxForeground\0"
/*  1251 */ "imagemapCallback\0"
/*  1268 */ "imagemapDrawBoundingBoxes\0"
/*  1294 */ "linkCallback\0"
/*  1307 */ "maxImageColors\0"
/*  1322 */ "mimeType\0"
/*  1331 */ "motionTrackCallback\0"
/*  1351 */ "parserCallback\0"
/*  1366 */ "parserIsProgressive\0"
/*  1386 */ "perfectColors\0"
/*  1400 */ "popdownDelay\0"
/*  1413 */ "popupDelay\0"
/*  1424 */ "progressiveEndProc\0"
/*  1443 */ "progressiveInitialDelay\0"
/*  1467 */ "progressiveMaximumDelay\0"
/*  1491 */ "progressiveMinimumDelay\0"
/*  1515 */ "progressiveReadProc\0"
/*  1535 */ "retainSource\0"
/*  1548 */ "screenGamma\0"
/*  1560 */ "strictHTMLChecking\0"
/*  1579 */ "topLine\0"
/*  1587 */ "transparent\0"
/*  1599 */ "uncompressCommand\0"
/* ????? */
"\0";

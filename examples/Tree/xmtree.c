#include <Xm/PushB.h>
#include <Xm/Tree.h>

#include <stdio.h>

const char *appName = "XsTreeTest";

int main(argc, argv)
int argc;
char *argv[];
{
  XtAppContext app;
  Display *theDisplay;

  XtToolkitInitialize();
  app = XtCreateApplicationContext();

  theDisplay = XtOpenDisplay (app, NULL, appName, appName, NULL, 0, &argc, argv);

  if(!theDisplay)
	printf("%s: can't open display, exiting...", appName);
  else {
	Widget toplevel = XtAppCreateShell(appName, appName, applicationShellWidgetClass, theDisplay, NULL, 0);
	Widget rootWidget = XsCreateTree(toplevel, (char *)"tree1", (Arg *)0, 0);
	Widget rootButton = XtVaCreateManagedWidget("Root", xmPushButtonWidgetClass, rootWidget, XmNsuperNode, NULL, NULL);

	int i = 0;
	while(++i<3) {
		Widget button =
			XtVaCreateManagedWidget(
				"Level 1", xmPushButtonWidgetClass, rootWidget,
				XmNsuperNode, rootButton,
				NULL
			);

		int j = 0;
		while(++j<4) {
			XtVaCreateManagedWidget(
				"Level 2", xmPushButtonWidgetClass, rootWidget,
				XmNsuperNode, button,
				NULL
			);
		}
	}

	XtManageChild(rootWidget);
	XtRealizeWidget(toplevel);
	XtAppMainLoop(app);
  }

  return 0;
}

#include <stdio.h>
#include <X11/Intrinsic.h>
#include <Xm/ListTree.h>

Widget	toplevel,paned,form,tree,list;
XtAppContext app_con;
char *files[256];


void
HighlightCallback(w,client,call)
Widget w;
XtPointer client;
XtPointer call;
{
ListTreeMultiReturnStruct *ret;
ListTreeItem *item;
int i;

	ret=(ListTreeMultiReturnStruct *)call;
	printf("HIGHLIGHT: count=%d\n",ret->count);
	for (i=0; i<ret->count; i++) {
	  item=ret->items[i];
	  printf("%s",item->text);
	  while (item->parent) {
	    item=item->parent;
	    printf("<--%s",item->text);
	  }
	  printf("\n");
	}
}

void
MenuCallback(w,client,call)
Widget w;
XtPointer client;
XtPointer call;
{
ListTreeItemReturnStruct *ret = (ListTreeItemReturnStruct *)call;

        printf ("MENU: item=%s\n", ret->item->text);
}

void
ActivateCallback(w,client,call)
Widget w;
XtPointer client;
XtPointer call;
{
ListTreeActivateStruct *ret;
/* ListTreeItem *item; */
int count;

	ret=(ListTreeActivateStruct *)call;
	printf("ACTIVATE: item=%s count=%d\n",ret->item->text,ret->count);
	count=0;
	while (count<ret->count) {
		printf(" path: %s\n",ret->path[count]->text);
		count++;
	}
}

void Init(Widget w_tree)
{
  int i,j,k,l;
  char test[64];
  ListTreeItem *level1,*level2,*level3,*level4;

  for (i=0; i<10; i++) {
	sprintf(test,"%c Level #1, entry #%d", (char)('Z'-i),i);
	level1=ListTreeAdd(w_tree,NULL,test);
	for (j=0; j<i; j++) {
		sprintf(test,"%c Level #2, entry #%d", (char)('Z'-j),j);
		level2=ListTreeAdd(w_tree,level1,test);
		for (k=0; k<i; k++) {
			sprintf(test,"%c Level #3, entry #%d", (char)('Z'-k),k);
			level3=ListTreeAdd(w_tree,level2,test);
			for (l=0; l<i; l++) {
				sprintf(test,"%c Level #4, entry #%d", (char)('Z'-l),l);
				level4=ListTreeAdd(w_tree,level3,test);
			}
		}
	}
  }
}

#include <X11/xpm.h>

static const char * folder_xpm[] = {
"17 13 4 1",
"       c white s Background",
".      c red",
"X      c black",
"o      c yellow",
"        .....    ",
"       .......X  ",
"  oooooooooooooX ",
" ooooooooooooooX ",
" ooooooooooooooX ",
" ooooooooooooooX ",
" ooooooooooooooX ",
" ooooooooooooooX ",
" ooooooooooooooX ",
" ooooooooooooooX ",
" ooooooooooooooX ",
"  ooooooooooooX  ",
"   XXXXXXXXXXX   "
};

static const char * folderopen_xpm[] = {
"17 13 5 1",
"       c white s Background",
".      c red",
"X      c black",
"o      c #FFFFCCCC4242",
"O      c yellow",
"         .....   ",
"        .......X ",
"   oooooooooooooX",
"  ooooooooooooooX",
"  ooooooooooooooX",
" OOOOOOOOOOOooooX",
"OOOOOOOOOOOOOoooX",
" OOOOOOOOOOOOOooX",
" OOOOOOOOOOOOOOoX",
"  OOOOOOOOOOOOOoX",
"  OOOOOOOOOOOOOoX",
"   OOOOOOOOOOOOX ",
"    XXXXXXXXXXX  "
};

static const char * fileopen_xpm[] = {
"17 13 5 1",
"       c white s Background",
".      c red",
"X      c black",
"o      c yellow",
"O      c #FFFFCCCC4242",
"     XXXXXXXXXXX ",
"    XXoooooooooX ",
"   XOXoooooooooX ",
"  XOOXoooooooooX ",
" XXXXXoooooooooX ",
" XoooooooooooooX ",
" XoooooooooooooX ",
" XoooooooooooooX ",
" XoooooooooooooX ",
" XoooooooooooooX ",
" XoooooooooooooX ",
" XoooooooooooooX ",
" XXXXXXXXXXXXXXX "
};

static const char * fileclosed_xpm[] = {
"17 13 5 1",
"       c white s Background",
".      c red",
"X      c black",
"o      c yellow",
"O      c #FFFFCCCC4242",
"     XXXXXXXXXXX ",
"    XXoooooooooX ",
"   XOXoooooooooX ",
"  XOOXoooooooooX ",
" XXXXXoooooooooX ",
" Xoooooo.ooooooX ",
" Xoooooo.ooooooX ",
" Xoooo.....ooooX ",
" Xoooooo.ooooooX ",
" Xoooooo.ooooooX ",
" XoooooooooooooX ",
" XoooooooooooooX ",
" XXXXXXXXXXXXXXX "
};

int main (argc, argv)
int argc;
char **argv;
{
Pixmap open,closed, file_open, file_closed; 
XpmColorSymbol transparent = {(char *)"Background", NULL, (Pixel)0};
XpmAttributes attrib;

  toplevel = XtAppInitialize(&app_con,"ListTreeDemo",NULL,0, &argc,argv,NULL,NULL,0);
  tree=XmCreateScrolledListTree(toplevel,(char *)"tree",NULL,0);

  XtVaGetValues(tree, XtNbackground, &transparent.pixel, NULL);

  attrib.colorsymbols=&transparent;
  attrib.valuemask=XpmColorSymbols;
  attrib.numsymbols=1;

  if(XpmCreatePixmapFromData(XtDisplay(toplevel),
    RootWindowOfScreen(XtScreen(toplevel)),
    (char **)folderopen_xpm,
    &open, NULL, &attrib) != XpmSuccess)
	fprintf(stderr, "Can't create pixmap \"open\"\n");

  if(XpmCreatePixmapFromData(XtDisplay(toplevel),
    RootWindowOfScreen(XtScreen(toplevel)),
    (char **)folder_xpm,
    &closed,NULL,&attrib) != XpmSuccess)
	fprintf(stderr, "Can't create pixmap \"closed\"\n");

  if(XpmCreatePixmapFromData(XtDisplay(toplevel),
    RootWindowOfScreen(XtScreen(toplevel)),
    (char **)fileopen_xpm,
    &file_open,NULL,&attrib) != XpmSuccess)
	fprintf(stderr, "Can't create pixmap \"fileopen\"\n");

  if(XpmCreatePixmapFromData(XtDisplay(toplevel),
    RootWindowOfScreen(XtScreen(toplevel)),
    (char **)fileclosed_xpm,
    &file_closed,NULL,&attrib) != XpmSuccess)
	fprintf(stderr, "Can't create pixmap \"fileclosed\"\n");

  XtVaSetValues(tree,
          XtNheight,      	(Dimension)200,
          XtNwidth,		(Dimension)150,
          XtNhorizontalSpacing, 5,
          XtNverticalSpacing,   5,
          XtNhighlightPath,     True,
          XtNbranchPixmap,	closed,
          XtNbranchOpenPixmap,	open,
          XtNleafPixmap,	file_closed,
          XtNleafOpenPixmap,	file_open,
          NULL);

  XtManageChild(tree);

  Init(tree);

  XtAddCallback(tree,XtNhighlightCallback, HighlightCallback, (XtPointer) NULL);
  XtAddCallback(tree,XtNactivateCallback, ActivateCallback, (XtPointer) NULL);
  XtAddCallback(tree,XtNmenuCallback, MenuCallback, (XtPointer) NULL);

  XtRealizeWidget(toplevel);

  XtAppMainLoop(app_con);

  return 0;
}

/*@
 * Copyright(c) 1995-1997 Gregory M. Messner
 * All rights reserved
 *
 * Permission to use, copy, modify and distribute this material for
 * non-commercial personal and educational use without fee is hereby
 * granted, provided that the above copyright notice and this permission 
 * notice appear in all copies, and that the name of Gregory M. Messner
 * not be used in advertising or publicity pertaining to this material
 * without the specific, prior written permission of Gregory M. Messner 
 * or an authorized representative.
 *
 * GREGORY M. MESSNER MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, 
 * EXPRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST INFRINGEMENT OF PATENTS
 * OR OTHER INTELLECTUAL PROPERTY RIGHTS. THE SOFTWARE IS PROVIDED "AS IS",
 * AND IN NO EVENT SHALL GREGORY M. MESSNER BE LIABLE FOR ANY DAMAGES,
 * INCLUDING ANY LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * RELATING TO THE SOFTWARE.
 *
 */


/*
 * Demonstartion code for XgTabs widget
 */


#include <stdio.h>
#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/Label.h>
#include <Xm/Frame.h>

#include <Xm/Tabs.h>

#define RES_CONVERT(r, v)   XtVaTypedArg, r, XmRString, v, strlen(v) + 1


/*
 * Text for the label in the center of the screen
 */
/* static String texts[] = { */
static const char * texts[] = {
"About the XgTabs Widget\n\
-----------------------\n\
\n\
You are looking at a frame with a XgTabs widget attached\n\
to each side. The XgTabs widget is \"NOT\" a manager, but\n\
rather an adornment. It does however have a resource\n\
which allows you to specify a list of widgets that it\n\
will manage/unmanage as the tabs are selected.\n\
\n\
In this example the top XgTabs widget manages a set of labels\n\
that present information associated with the selected tab.\n\
A more common use would be for the XgTabs widget to manage a set\n\
of XmForm widgets which would contain the dialog controls for the\n\
specific tab.\n\
\n\
\n\
Click on the \'Quit\' tab to exit.",

"Using XgTabs\n\
------------\n\
\n\
You can select a tab by clicking on it with mouse\n\
button 1.  The keyboard can also be used to traverse\n\
a XgTabs widget. Use the arrow keys to move the highlight\n\
from one tab to the other, and select the desired tab by\n\
pressing the [Space Bar].  The [Tab] key will the move\n\
focus from the XgTabs widget to the next widget in a Motif\n\
tab group.\n\
\n\
\n\
Click on the \'Quit\' tab to exit.",


"XgTabs Widget Resources\n\
\n\
New (Xg) Resources           Old (Xm) Resources\n\
------------------           ---------------------\n\
XgNactivateCallback          XmNactivateCallback\n\
XgNautoUnmanage              XmNautoUnmanage\n\
XgNcurrentTab\n\
XgNfocusTab\n\
XgNfontList                  XmNfontList\n\
XgNhighlightThickness        XmNhighlightThickness\n\
XgNlabelMargin\n\
XgNnavigationType            XmNnavigationType\n\
XgNselectedTabColor\n\
XgNtabColor\n\
XgNtabCount\n\
XgNtabLables\n\
XgNtabLocation\n\
XgNtabShape\n\
XgNtabWidgets\n\
\n\
All Core and XmPrimitive Resources\n\
\n\
Click on the \'Quit\' tab to exit.",

"\
Copyright(c) 1995-1997 Gregory M. Messner\n\
All rights reserved\n\
\n\
Permission to use, copy, modify and distribute this material for\n\
non-commercial personal and educational use without fee is hereby\n\
granted, provided that the above copyright notice and this permission\n\
notice appear in all copies, and that the name of Gregory M. Messner\n\
not be used in advertising or publicity pertaining to this material\n\
without the specific, prior written permission of Gregory M. Messner\n\
or an authorized representative.\n\
\n\
GREGORY M. MESSNER MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES,\n\
EXPRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT NOT\n\
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR\n\
ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST INFRINGEMENT OF PATENTS\n\
OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE SOFTWARE IS PROVIDED \"AS IS\",\n\
AND IN NO EVENT SHALL GREGORY M. MESSNER BE LIABLE FOR ANY DAMAGES,\n\
INCLUDING ANY LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES\n\
RELATING TO THE SOFTWARE.",


"Press the \"Quit\" tab again to exit."
};


/*
 * The labels for the top set of Tabs
 */
/* static String labels[] = { */
static const char * labels[] = {
    "About",
    "Using",
    "Resources",
    "License",
    "Quit"
};

/*
 * Labels for other tabs
 */
/* static String alphabet[] = { */
static const char * alphabet[] = {
    "AB",
    "CD",
    "EF",
    "GH",
    "IJ",
    "KL",
    "MN",
    "OP",
    "QR",
    "ST",
    "UV",
    "WZ",
};

/* static String monthlabels[] = { */
static const char * monthlabels[] = {
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec"
};


static void handle_tab_click(w, label, call_data_xptr)
    Widget w;
    XtPointer label;
    XtPointer call_data_xptr;
{
    XgTabsCallbackStruct *call_data = (XgTabsCallbackStruct *)call_data_xptr;

    if ( call_data->tab != call_data->old_tab )
	XtVaSetValues((Widget)label, RES_CONVERT(XmNlabelString, 
	    texts[call_data->tab]), NULL);

    /*
     * If the "Quit" tab was selected, exit
     */
    if ( call_data->tab == XtNumber(labels) - 1 && 
		call_data->tab == call_data->old_tab )
			exit(0);
}


int main(argc, argv)
    int argc;
    String *argv;
{
Widget frame, form, toplevel, toptab, bottomtab, label, righttab, lefttab;
XtAppContext app_context;


    /*
     * Initialize the toolkit and create a top level shell.
     */
    toplevel = XtVaAppInitialize(&app_context, argv[0], NULL, 0,
		  &argc, argv, NULL, NULL);

    /*
     * Create a form to hold everything.
     */
    form = XtVaCreateWidget
	("form", xmFormWidgetClass, toplevel, 
	XmNorientation, XmVERTICAL,
	XmNpacking, XmPACK_TIGHT, NULL);


    /*
     * Create a frame. It will hold the text for the demo and will
     * be surrounded by a XgTabs widget on each side
     */
    frame = XtVaCreateManagedWidget(
    	"frame", xmFrameWidgetClass, form,
	XmNshadowType, XmSHADOW_OUT,
	XmNshadowThickness, 1,
    	XmNleftAttachment, XmATTACH_FORM,
	XmNleftOffset, 60,
	XmNtopAttachment, XmATTACH_FORM,
	XmNtopOffset, 60,
	XmNrightAttachment, XmATTACH_FORM,
        XmNrightOffset, 60,
	XmNbottomAttachment, XmATTACH_FORM,
	XmNbottomOffset, 60,
	NULL);

	
    /*
     * Create a left side XgTabs widget
     */
    lefttab = XtVaCreateManagedWidget("tabs", xgTabsWidgetClass, form, 
    	XmNtopAttachment, XmATTACH_FORM,
	XmNtopOffset, 60,
	XmNleftAttachment, XmATTACH_NONE,
	XmNleftOffset, 0,
	XmNrightAttachment, XmATTACH_OPPOSITE_FORM,
	XmNrightOffset, -60,
	XmNbottomAttachment, XmATTACH_FORM,
	XmNbottomOffset, 60,
	RES_CONVERT(XgNtabColor, "Gray75"),
	RES_CONVERT(XmNforeground, "black"),
	RES_CONVERT(XmNfontList, "9x15bold"),
	XgNtabLocation, XgLEFT,
	XgNtabShape, XgSLANTED,
	RES_CONVERT(XgNselectedTabColor, "cyan"),
	XgNtabLabels, alphabet,
	XgNlabelMargin, 3,
	XgNcurrentTab, 0,
	XgNtabCount, XtNumber(alphabet),
	XmNhighlightThickness, 2,
	XmNshadowThickness, 3,
	NULL);


    /*
     * Create a right side XgTabs widget
     */
    righttab = XtVaCreateManagedWidget("tabs", xgTabsWidgetClass, form, 
    	XmNtopAttachment, XmATTACH_FORM,
	XmNtopOffset, 60,
	XmNleftAttachment, XmATTACH_OPPOSITE_FORM,
	XmNleftOffset, -60,
	XmNrightAttachment, XmATTACH_NONE,
	XmNbottomAttachment, XmATTACH_FORM,
	XmNbottomOffset, 60,
	RES_CONVERT(XgNtabColor, "Gray75"),
	RES_CONVERT(XmNforeground, "black"),
	RES_CONVERT(XmNfontList, "9x15bold"),
	RES_CONVERT(XgNselectedTabColor, "yellow"),
	XgNtabLocation, XgRIGHT,
	XgNtabShape, XgCHAMFERRED,
	XgNtabLabels, monthlabels,
	XgNcurrentTab, 0,
	XgNtabCount, XtNumber(monthlabels),
        XmNshadowThickness, 2,
        XmNhighlightThickness, 1,
	NULL);

    /*
     * Create the label that will hold the text for the demo
     */
    label = XtVaCreateManagedWidget("label", xmLabelWidgetClass, frame,
	     XmNalignment, XmALIGNMENT_BEGINNING,
	     RES_CONVERT(XmNlabelString, texts[0]),
	     XmNmarginWidth, 60,
	     XmNmarginHeight, 60,
	     NULL);

    /*
     * Create the top side XgTabs widget. We add an activate handler
     * that will switch the text diplayed in the just created 
     * label widget
     */
    toptab = XtVaCreateManagedWidget("tabs", xgTabsWidgetClass, form, 
    	XmNtopAttachment, XmATTACH_NONE,
	XmNleftAttachment, XmATTACH_FORM,
	XmNleftOffset, 60,
	XmNrightAttachment, XmATTACH_FORM,
	XmNrightOffset, 60,
	XmNbottomAttachment, XmATTACH_WIDGET,
	XmNbottomWidget, frame,
	RES_CONVERT(XgNtabColor, "gray75"),
	RES_CONVERT(XmNforeground, "black"),
	RES_CONVERT(XmNfontList, "12x24"),
	RES_CONVERT(XgNselectedTabColor, "green"),
	XgNtabLocation, XgTOP,
	XgNtabShape, XgSLANTED,
	XgNtabLabels, labels,
	XgNcurrentTab, 0,
	XgNtabCount, XtNumber(labels),
	XmNshadowThickness, 3,
	XmNhighlightThickness, 2,
	NULL);

    XtAddCallback(toptab, XmNactivateCallback, handle_tab_click, 
    	(XtPointer)label);


    /*
     * Create the bottom side XgTabs widget
     */
    bottomtab = XtVaCreateManagedWidget("tabs", xgTabsWidgetClass, form, 
    	XmNbottomAttachment, XmATTACH_NONE,
	XmNleftAttachment, XmATTACH_FORM,
	XmNleftOffset, 60,
	XmNrightAttachment, XmATTACH_FORM,
	XmNrightOffset, 60,
	XmNtopAttachment, XmATTACH_WIDGET,
	XmNtopWidget, frame,
	RES_CONVERT(XgNtabColor, "Pink"),
	RES_CONVERT(XmNforeground, "black"),
	RES_CONVERT(XmNfontList, "12x24"),
	XgNtabLocation, XgBOTTOM,
	XgNtabShape, XgCHAMFERRED,
	XgNtabLabels, alphabet,
	XgNcurrentTab, 3,
	XgNtabCount, XtNumber(alphabet),
	XgNlabelMargin, 6,
	XmNhighlightThickness, 2,
	XmNshadowThickness, 2,
	NULL);


    /*
     * Manage the form (this will cause it to be displayed
     */
    XtManageChild(form);


    /*
     * Finally realize the top level shell and enter the main loop
     */
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app_context);

    return 0;
}

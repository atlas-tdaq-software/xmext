/*****
* menuItems.h : example_2 menu item definitions
*
* This file Version	$Revision$
*
* Creation date:		Sun Dec 14 17:16:11 GMT+0100 1997
* Last modification: 	$Date$
* By:					$Author$
* Current State:		$State$
*
* Author:				newt
*
* Copyright (C) 1994-1997 by Ripley Software Development 
* All Rights Reserved
*
* This file is part of the XmHTML Widget Library.
*
* This library is free software; you can redistribute it and/or
* modify it under the terms of the GNU [Library] General Public
* License as published by the Free Software Foundation; either
* version 2 of the License, or (at your option) any later version.
*
* This library is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
* Library General Public License for more details.
*
* You should have received a copy of the GNU [Library] General Public
* License along with this library; if not, write to the Free
* Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
*****/
/*****
* $Source$
*****/
/*****
* ChangeLog 
* $Log$
* Revision 1.1  2002/11/22 14:08:00  akazarov
* Initial revision
*
* Revision 1.1  1999/02/11 18:59:01  isolov
* add examples for XbaeMatrix and XmHTML widgets
*
*****/ 

#ifndef _menuItems_h_
#define _menuItems_h_

#include <Xm/CascadeBG.h>
#include <Xm/ToggleBG.h>
#include <Xm/PushBG.h>
#include <Xm/SeparatoG.h>

/****
* Menu defines
*****/

/* File Menu defines */
#define FILE_NEW						0
#define FILE_OPEN						1
#define FILE_OPEN_URL					2
#define FILE_SAVEAS						3
#define FILE_RELOAD						4
#define FILE_VIEW						5
#define FILE_PRINT						6
#define FILE_CLOSE						7
#define FILE_QUIT						8

/* File->View defines */
#define VIEW_INFO						0
#define VIEW_SOURCE						1
#define VIEW_FONTS						2
#define VIEW_STRUCTURE					3

/* Edit Menu defines */
#define EDIT_FIND						0
#define EDIT_FIND_AGAIN					1

/* Option Menu defines */
#define OPTIONS_GENERAL					0
#define OPTIONS_DOCUMENT				1
#define OPTIONS_ANCHOR					2
#define OPTIONS_IMAGES					3
#define OPTIONS_FONTS					4

#define OPTIONS_START					5

#define OPTIONS_ANCHOR_BUTTONS			( OPTIONS_START )
#define OPTIONS_ANCHOR_HIGHLIGHT		( OPTIONS_START + 1 )
#define OPTIONS_ANCHOR_TRACKING			( OPTIONS_START + 2 )
#define OPTIONS_ANCHOR_TIPS				( OPTIONS_START + 3 )

#define OPTIONS_BODY_COLORS				( OPTIONS_START + 4 )
#define OPTIONS_BODY_IMAGES				( OPTIONS_START + 5 )
#define OPTIONS_ALLOW_COLORS			( OPTIONS_START + 6 )
#define OPTIONS_ALLOW_FONTS				( OPTIONS_START + 7 )
#define OPTIONS_JUSTIFY					( OPTIONS_START + 8 )

#define OPTIONS_STRICTHTML				( OPTIONS_START + 9 )
#define OPTIONS_BADHTML					( OPTIONS_START + 10 )

#define OPTIONS_ANIMATION_FREEZE		( OPTIONS_START + 11 )
#define OPTIONS_ENABLE_IMAGES			( OPTIONS_START + 12 )
#define OPTIONS_AUTOLOAD_IMAGES			( OPTIONS_START + 13 )
#define OPTIONS_SAVE					( OPTIONS_START + 14 )

#define OPTIONS_LAST					( OPTIONS_START + 15 )

/* Warning menu defines correspond with XmHTML warning type defines */

/* Window Menu defines */
#define WINDOW_RAISE					0
#define WINDOW_LOWER					1

/* Help Menu defines */
#define HELP_ABOUT						0

/* Any menu items that are commented out aren't supported (yet) */
static MenuItem viewMenu[] = {
	{(char *)"viewInfo", &xmPushButtonGadgetClass, None, True, (XtPointer)VIEW_INFO,
		viewCB, NULL, NULL },
	{(char *)"viewSource", &xmPushButtonGadgetClass, None, True, (XtPointer)VIEW_SOURCE,
		viewCB, NULL, NULL },
	{(char *)"viewFonts", &xmPushButtonGadgetClass, None, True, (XtPointer)VIEW_FONTS,
		viewCB, NULL, NULL },
	{(char *)"viewStructure", &xmPushButtonGadgetClass, None, True,
		(XtPointer)VIEW_STRUCTURE, viewCB, NULL, NULL },
	{NULL, NULL, None, True, NULL, NULL, NULL, NULL },
};

static MenuItem fileMenu[] = {
#if 0
	{(char *)"new", &xmPushButtonGadgetClass, None, False, (XtPointer)FILE_NEW,
		fileCB, NULL, NULL },
	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },
#endif
	{(char *)"open", &xmPushButtonGadgetClass, None, True, (XtPointer)FILE_OPEN,
		fileCB, NULL, NULL },
#if 0
	{(char *)"openURL", &xmPushButtonGadgetClass, None, False, (XtPointer)FILE_OPEN_URL,
		fileCB, NULL, NULL },
#endif
	{(char *)"saveas", &xmPushButtonGadgetClass, None, True, (XtPointer)FILE_SAVEAS,
		fileCB, NULL, NULL },
	{(char *)"reload", &xmPushButtonGadgetClass, None, False, (XtPointer)FILE_RELOAD,
		fileCB, NULL, NULL },
	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },
	{(char *)"view", &xmCascadeButtonGadgetClass, None, True, (XtPointer)FILE_VIEW,
		fileCB, NULL, viewMenu},
	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },
#if 0
	{(char *)"print", &xmPushButtonGadgetClass, None, False, (XtPointer)FILE_PRINT,
		fileCB, NULL, NULL },
	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },
	{(char *)"close", &xmPushButtonGadgetClass, None, False, (XtPointer)FILE_CLOSE,
		fileCB, NULL, NULL },
#endif
	{(char *)"quit", &xmPushButtonGadgetClass, None, True, (XtPointer)FILE_QUIT,
		fileCB, NULL, NULL },
	{NULL, NULL, None, True, NULL, NULL, NULL, NULL },
}; 

static MenuItem warningMenu[] = {
	{(char *)"none", &xmToggleButtonGadgetClass, None, False, (XtPointer)XmHTML_NONE,
		warningCB, NULL, NULL },
	{(char *)"all", &xmToggleButtonGadgetClass, None, True, (XtPointer)XmHTML_ALL,
		warningCB, NULL, NULL },
	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },
	{(char *)"unknownElement", &xmToggleButtonGadgetClass, None, False,
		(XtPointer)XmHTML_UNKNOWN_ELEMENT, warningCB, NULL, NULL },
	{(char *)"bad", &xmToggleButtonGadgetClass, None, False,
		(XtPointer)XmHTML_BAD, warningCB, NULL, NULL },
	{(char *)"openBlock", &xmToggleButtonGadgetClass, None, False,
		(XtPointer)XmHTML_OPEN_BLOCK,
		warningCB, NULL, NULL },
	{(char *)"closeBlock", &xmToggleButtonGadgetClass, None, False,
		(XtPointer)XmHTML_CLOSE_BLOCK,
		warningCB, NULL, NULL },
	{(char *)"openElement", &xmToggleButtonGadgetClass, None, False,
		(XtPointer)XmHTML_OPEN_ELEMENT, warningCB, NULL, NULL },
	{(char *)"nested", &xmToggleButtonGadgetClass, None, False,
		(XtPointer)XmHTML_NESTED, warningCB, NULL, NULL },
	{(char *)"violation", &xmToggleButtonGadgetClass, None, False,
		(XtPointer)XmHTML_VIOLATION, warningCB, NULL, NULL },
	{NULL, NULL, None, True, NULL, NULL, NULL, NULL },
}; 

static MenuItem optionMenu[] = {
#if 0
	{(char *)"general", &xmPushButtonGadgetClass, None, False, 
		(XtPointer)OPTIONS_GENERAL, optionsCB, NULL, NULL },
	{(char *)"document", &xmPushButtonGadgetClass, None, False,
		(XtPointer)OPTIONS_DOCUMENT, optionsCB, NULL, NULL },
	{(char *)"anchor", &xmPushButtonGadgetClass, None, False,
		(XtPointer)OPTIONS_ANCHOR, optionsCB, NULL, NULL },
	{(char *)"images", &xmPushButtonGadgetClass, None, False,
		(XtPointer)OPTIONS_IMAGES, optionsCB, NULL, NULL },
	{(char *)"fonts", &xmPushButtonGadgetClass, None, False,
		(XtPointer)OPTIONS_FONTS, optionsCB, NULL, NULL },

	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },
#endif

	/* anchor options */
	{(char *)"anchorButtons", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_ANCHOR_BUTTONS, optionsCB, NULL, NULL },
	{(char *)"highlightOnEnter", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_ANCHOR_HIGHLIGHT, optionsCB, NULL, NULL },
	{(char *)"imageAnchorTracking", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_ANCHOR_TRACKING, optionsCB, NULL, NULL },
	{(char *)"anchorTips", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_ANCHOR_TIPS, optionsCB, NULL, NULL },

	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },

	/* body options */
	{(char *)"enableBodyColors", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_BODY_COLORS, optionsCB, NULL, NULL },
	{(char *)"enableBodyImages", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_BODY_IMAGES, optionsCB, NULL, NULL },
	{(char *)"enableDocumentColors", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_ALLOW_COLORS, optionsCB, NULL, NULL },
	{(char *)"enableDocumentFonts", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_ALLOW_FONTS, optionsCB, NULL, NULL },
	{(char *)"enableOutlining", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_JUSTIFY, optionsCB, NULL, NULL },

	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },

	/* parser options */
	{(char *)"strictHTMLChecking", &xmToggleButtonGadgetClass, None, False,
		(XtPointer)OPTIONS_STRICTHTML, optionsCB, NULL, NULL },
	{(char *)"warning", &xmCascadeButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_BADHTML, optionsCB, NULL, warningMenu},

	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },

	/* image options */
	{(char *)"freezeAnimations", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_ANIMATION_FREEZE, optionsCB, NULL, NULL },
	{(char *)"imageEnable", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_ENABLE_IMAGES, optionsCB, NULL, NULL },
	{(char *)"autoImageLoad", &xmToggleButtonGadgetClass, None, True,
		(XtPointer)OPTIONS_AUTOLOAD_IMAGES, optionsCB, NULL, NULL },

	{(char *)"_separator_", &xmSeparatorGadgetClass, None, True, NULL,
		NULL, NULL, NULL },

	{(char *)"save", &xmPushButtonGadgetClass, None, False,
		(XtPointer)OPTIONS_SAVE, optionsCB, NULL, NULL },
	{NULL, NULL, None, True, NULL, NULL, NULL, NULL },
};

/* the Edit menu */
static MenuItem editMenu[] = {
	{(char *)"find", &xmPushButtonGadgetClass, None, True,
		(XtPointer)EDIT_FIND, editCB, NULL, NULL },
	{(char *)"findAgain", &xmPushButtonGadgetClass, None, True,
		(XtPointer)EDIT_FIND_AGAIN, editCB, NULL, NULL },
	{NULL, NULL, None, True, NULL, NULL, NULL, NULL },
};

/* the Window menu */
static MenuItem windowMenu[] = {
	{(char *)"raise", &xmPushButtonGadgetClass, None, True,
		(XtPointer)WINDOW_RAISE, windowCB, NULL, NULL },
	{(char *)"lower", &xmPushButtonGadgetClass, None, True,
		(XtPointer)WINDOW_LOWER, windowCB, NULL, NULL },
	{NULL, NULL, None, True, NULL, NULL, NULL, NULL },
};

/* the Help menu */
static MenuItem helpMenu[] = {
	{(char *)"about", &xmPushButtonGadgetClass, None, True,
		(XtPointer)HELP_ABOUT, helpCB, NULL, NULL },
	{NULL, NULL, None, True, NULL, NULL, NULL, NULL },
};

/* Don't add anything after this endif! */
#endif /* _menuItems_h_ */

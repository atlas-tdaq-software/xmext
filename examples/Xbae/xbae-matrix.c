/*
 * AUTHOR: Jay Schmidgall <jay.schmidgall@spdbump.sungardss.com>
 *
 * $Id$
 */

#include <Xm/Matrix.h>

#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/RowColumn.h>
#include <Xm/Scale.h>
#include <Xm/ToggleB.h>
#include <Xm/TextF.h>
#include <Xm/Text.h>

#include <stdio.h>
#include <string.h>

void
LoadMatrix(w)
Widget w;
{
    static String rows[10][10] = {
	{ (String)"0,Zero", (String)"0,One",  (String)"0,Two",    (String)"0,Three",  (String)"0,Four",
	  (String)"0,Five", (String)"0,Six",  (String)"0, Seven", (String)"0, Eight", (String)"0, Nine" },
	{ (String)"1,Zero", (String)"1,One",  (String)"1,Two",    (String)"1,Three",  (String)"1,Four",
	  (String)"1,Five", (String)"1,Six",  (String)"1, Seven", (String)"1, Eight", (String)"1, Nine" },
	{ (String)"2,Zero", (String)"2,One",  (String)"2,Two",    (String)"2,Three",  (String)"2,Four",
	  (String)"2,Five", (String)"2,Six",  (String)"2, Seven", (String)"2, Eight", (String)"2, Nine" },
	{ (String)"3,Zero", (String)"3,One",  (String)"3,Two",    (String)"3,Three",  (String)"3,Four",
	  (String)"3,Five", (String)"3,Six",  (String)"3, Seven", (String)"3, Eight", (String)"3, Nine" },
	{ (String)"4,Zero", (String)"4,One",  (String)"4,Two",    (String)"4,Three",  (String)"4,Four",
	  (String)"4,Five", (String)"4,Six",  (String)"4, Seven", (String)"4, Eight", (String)"4, Nine" },
	{ (String)"5,Zero", (String)"5,One",  (String)"5,Two",    (String)"5,Three",  (String)"5,Four",
	  (String)"5,Five", (String)"5,Six",  (String)"5, Seven", (String)"5, Eight", (String)"5, Nine" },
	{ (String)"6,Zero", (String)"6,One",  (String)"6,Two",    (String)"6,Three",  (String)"6,Four",
	  (String)"6,Five", (String)"6,Six",  (String)"6, Seven", (String)"6, Eight", (String)"6, Nine" },
	{ (String)"7,Zero", (String)"7,One",  (String)"7,Two",    (String)"7,Three",  (String)"7,Four",
	  (String)"7,Five", (String)"7,Six",  (String)"7, Seven", (String)"7, Eight", (String)"7, Nine" },
	{ (String)"8,Zero", (String)"8,One",  (String)"8,Two",    (String)"8,Three",  (String)"8,Four",
	  (String)"8,Five", (String)"8,Six",  (String)"8, Seven", (String)"8, Eight", (String)"8, Nine" },
	{ (String)"9,Zero", (String)"9,One",  (String)"9,Two",    (String)"9,Three",  (String)"9,Four",
	  (String)"9,Five", (String)"9,Six",  (String)"9, Seven", (String)"9, Eight", (String)"9, Nine" }
    };

    static String columnLabels[] =
	{ (String)"Zero", (String)"One", (String)"Two",   (String)"Three", (String)"Four",
          (String)"Five", (String)"Six", (String)"Seven", (String)"Eight", (String)"Nine"};

    short columnWidths[] =
	{ 6, 5, 5, 7, 6, 6, 5, 8, 8, 7 };

    static String rowLabels[] =
	{ (String)"0", (String)"1", (String)"2", (String)"3", (String)"4",
	  (String)"5", (String)"6", (String)"7", (String)"8", (String)"9"};

    String *cells[10];

    cells[0] = &rows[0][0];
    cells[1] = &rows[1][0];
    cells[2] = &rows[2][0];
    cells[3] = &rows[3][0];
    cells[4] = &rows[4][0];
    cells[5] = &rows[5][0];
    cells[6] = &rows[6][0];
    cells[7] = &rows[7][0];
    cells[8] = &rows[8][0];
    cells[9] = &rows[9][0];

    
    XtVaSetValues(w,
	XmNcells,		cells,
	XmNcolumns,		10,
	XmNvisibleColumns,	5,
	XmNcolumnLabels,	columnLabels,
	XmNcolumnWidths,	columnWidths,
	XmNrows,		10,
	XmNvisibleRows,		5,
	XmNrowLabels,		rowLabels,
	XmNtraverseFixedCells,	True,
	NULL);
}

typedef struct {
    Widget  mw;
    char    *resource;
    int	    value;
} SetValueStruct;


void
cbRadio(w, client, call)
Widget w;
XtPointer client;
XtPointer call;
{
    XmToggleButtonCallbackStruct *cbs = (XmToggleButtonCallbackStruct *) call;
    SetValueStruct *svs = (SetValueStruct *) client;

    if (cbs->set)
	XtVaSetValues(svs->mw, svs->resource, svs->value, NULL);
}


void
cbScale(w, client, call)
Widget w;
XtPointer client;
XtPointer call;
{
    XmScaleCallbackStruct *cbs = (XmScaleCallbackStruct *) call;
    SetValueStruct *svs = (SetValueStruct *) client;
    XtVaSetValues(svs->mw, svs->resource, cbs->value, NULL);
}


Widget
createButtonBox(parent, name)
Widget parent;
char *name;
{
    Widget frame, frame2, label, rc;
    
    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, parent,
	NULL);

    frame2 = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, frame,
	XmNchildType,		    XmFRAME_TITLE_CHILD,
	XmNchildHorizontalAlignment,   XmALIGNMENT_CENTER,
	XmNchildVerticalAlignment,	    XmALIGNMENT_CENTER,
	XmNmarginWidth,		    4,
	NULL);
    
    label = XtVaCreateManagedWidget(
	name, xmLabelWidgetClass, frame2,
	NULL);
    
    rc = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, frame,
	XmNchildType,	    XmFRAME_WORKAREA_CHILD,
	XmNadjustLast,	    False,
	XmNradioBehavior,  True,
	XmNradioAlwaysOne, True,
	XmNorientation,    XmHORIZONTAL,
	XmNnumColumns,	    3,
	XmNpacking,	    XmPACK_COLUMN,
	NULL);
    return rc;
}


Widget
createScaleBox(parent, name, min, max)
Widget parent;
char *name;
int min, max;
{
    Widget frame, frame2, label, scale;

    frame = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, parent,
				    NULL);

    frame2 = XtVaCreateManagedWidget(
	"frame", xmFrameWidgetClass, frame,
	XmNchildType,		    XmFRAME_TITLE_CHILD,
	XmNchildHorizontalAlignment,   XmALIGNMENT_CENTER,
	XmNchildVerticalAlignment,	    XmALIGNMENT_CENTER,
	XmNmarginWidth,		    4,
	NULL);

    label = XtVaCreateManagedWidget(
	name, xmLabelWidgetClass, frame2,
	NULL);
    
    scale = XtVaCreateManagedWidget(
	"scale", xmScaleWidgetClass, frame,
	XmNchildType,   XmFRAME_WORKAREA_CHILD,
	XmNorientation, XmHORIZONTAL,
	XmNminimum,	    min,
	XmNmaximum,	    max,
	XmNscaleMultiple, 1,
	XmNshowValue,   True,
	NULL);
    return scale;
}


void
createGridType(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNgridType);

    rc = createButtonBox(parent, "Grid Type");

    button = XtVaCreateManagedWidget(
	"XmGRID_NONE", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmGRID_NONE;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmGRID_LINE", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmGRID_LINE;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmGRID_SHADOW_IN", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmGRID_SHADOW_IN;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmGRID_SHADOW_OUT", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmGRID_SHADOW_OUT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmGRID_ROW_SHADOW", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmGRID_ROW_SHADOW;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmGRID_COLUMN_SHADOW", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmGRID_COLUMN_SHADOW;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}


void
createCellShadowType(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNcellShadowType);

    rc = createButtonBox(parent, "Cell Shadow Type");
    XtVaSetValues(rc,
		  XmNnumColumns, 1,
		  XmNorientation, XmVERTICAL,
		  XmNpacking, XmPACK_TIGHT,
		  NULL);

    button = XtVaCreateManagedWidget(
	"XmSHADOW_IN", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmSHADOW_IN;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmSHADOW_OUT", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmSHADOW_OUT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmSHADOW_ETCHED_IN", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmSHADOW_ETCHED_IN;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmSHADOW_ETCHED_OUT", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmSHADOW_ETCHED_OUT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}
    

void
createShadowType(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNshadowType);

    rc = createButtonBox(parent, "Shadow Type");
    XtVaSetValues(rc,
		  XmNnumColumns, 1,
		  XmNorientation, XmVERTICAL,
		  XmNpacking, XmPACK_TIGHT,
		  NULL);


    button = XtVaCreateManagedWidget(
	"XmSHADOW_IN", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmSHADOW_IN;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmSHADOW_OUT", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmSHADOW_OUT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmSHADOW_ETCHED_IN", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmSHADOW_ETCHED_IN;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmSHADOW_ETCHED_OUT", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmSHADOW_ETCHED_OUT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}
    

void
createPlacement(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNscrollBarPlacement);

    rc = createButtonBox(parent, "Scrollbar Placement");

    button = XtVaCreateManagedWidget(
	"XmTOP_LEFT", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmTOP_LEFT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);

    button = XtVaCreateManagedWidget(
	"XmTOP_RIGHT", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmTOP_RIGHT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);

    button = XtVaCreateManagedWidget(
	"XmBOTTOM_LEFT", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmBOTTOM_LEFT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);

    button = XtVaCreateManagedWidget(
	"XmBOTTOM_RIGHT", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmBOTTOM_RIGHT;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}
    

void
createFill(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNfill);

    rc = createButtonBox(parent, "Fill");
    XtVaSetValues(rc, XmNnumColumns, 1, NULL);

    button = XtVaCreateManagedWidget(
	"True", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = True;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"False", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = False;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}
    

void
createColumnResize(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNallowColumnResize);

    rc = createButtonBox(parent, "Allow Column Resize");
    XtVaSetValues(rc, XmNnumColumns, 1, NULL);

    button = XtVaCreateManagedWidget(
	"True", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = True;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"False", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = False;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}
    
void
createButtonLabels(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNbuttonLabels);

    rc = createButtonBox(parent, "Button Labels");
    XtVaSetValues(rc, XmNnumColumns, 1, NULL);

    button = XtVaCreateManagedWidget(
	"True", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = True;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"False", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = False;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}

void
createVerticalDisplay(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNverticalScrollBarDisplayPolicy);

    rc = createButtonBox(parent, "Vertical Display Policy");
    XtVaSetValues(rc,
		  XmNnumColumns, 1,
		  XmNorientation, XmVERTICAL,
		  XmNpacking, XmPACK_TIGHT,
		  NULL);

    button = XtVaCreateManagedWidget(
	"XmDISPLAY_NONE", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmDISPLAY_NONE;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmDISPLAY_AS_NEEDED", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmDISPLAY_AS_NEEDED;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);

    button = XtVaCreateManagedWidget(
	"XmDISPLAY_STATIC", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmDISPLAY_STATIC;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}
    

void
createHorizontalDisplay(parent, mw)
Widget parent;
Widget mw;
{
    Widget rc, button;
    SetValueStruct *svs;
    String resource = XtNewString(XmNhorizontalScrollBarDisplayPolicy);

    rc = createButtonBox(parent, "Horizontal Display Policy");
    XtVaSetValues(rc,
		  XmNnumColumns, 1,
		  XmNorientation, XmVERTICAL,
		  XmNpacking, XmPACK_TIGHT,
		  NULL);

    button = XtVaCreateManagedWidget(
	"XmDISPLAY_NONE", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmDISPLAY_NONE;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
    
    button = XtVaCreateManagedWidget(
	"XmDISPLAY_AS_NEEDED", xmToggleButtonWidgetClass, rc,
	XmNset, True,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmDISPLAY_AS_NEEDED;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);

    button = XtVaCreateManagedWidget(
	"XmDISPLAY_STATIC", xmToggleButtonWidgetClass, rc,
	NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    svs->value = XmDISPLAY_STATIC;
    XtAddCallback(button, XmNvalueChangedCallback, cbRadio, (XtPointer) svs);
}
    

void
createCellShadowThickness(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNcellShadowThickness);

    scale = createScaleBox(parent, "Cell Shadow Thickness", 0, 10);
    XtVaSetValues(scale, XmNvalue, 2, NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createShadowThickness(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNshadowThickness);

    scale = createScaleBox(parent, "Shadow Thickness", 0, 10);
    XtVaSetValues(scale, XmNvalue, 2, NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createFixedRows(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNfixedRows);

    scale = createScaleBox(parent, "Fixed Rows", 0, 4);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createFixedColumns(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNfixedColumns);

    scale = createScaleBox(parent, "Fixed Columns", 0, 4);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createTrailingFixedRows(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNtrailingFixedRows);

    scale = createScaleBox(parent, "Trailing Fixed Rows", 0, 4);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createTrailingFixedColumns(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNtrailingFixedColumns);

    scale = createScaleBox(parent, "Trailing Fixed Columns", 0, 4);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createCellMarginWidth(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNcellMarginWidth);

    scale = createScaleBox(parent, "Cell Margin Width", 0, 10);
    XtVaSetValues(scale, XmNvalue, 5, NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createCellMarginHeight(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNcellMarginHeight);

    scale = createScaleBox(parent, "Cell Margin Height", 0, 10);
    XtVaSetValues(scale, XmNvalue, 5, NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createCellHighlightThickness(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNcellHighlightThickness);

    scale = createScaleBox(parent, "Cell Highlight Thickness", 0, 10);
    XtVaSetValues(scale, XmNvalue, 2, NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
createSpace(parent, mw)
Widget parent;
Widget mw;
{
    Widget scale;
    SetValueStruct *svs;
    String resource = XtNewString(XmNspace);

    scale = createScaleBox(parent, "Space", 0, 10);
    XtVaSetValues(scale, XmNvalue, 4, NULL);
    svs = XtNew(SetValueStruct);
    svs->mw = mw;
    svs->resource = resource;
    XtAddCallback(scale, XmNvalueChangedCallback, cbScale, (XtPointer) svs);
}


void
cbSelect(w, client, call)
Widget w;
XtPointer client;
XtPointer call;
{
    int x, y;
    char buf[200];
    Widget text = (Widget) client;
    XbaeMatrixWidget mw;
    XbaeMatrixSelectCellCallbackStruct *cbs =
	(XbaeMatrixSelectCellCallbackStruct*) call;

    if (XtIsSubclass(w, xbaeMatrixWidgetClass))
	mw = ( XbaeMatrixWidget )w;
    else if (XtIsSubclass(XtParent(w), xbaeMatrixWidgetClass))
	mw = ( XbaeMatrixWidget )XtParent(w);
    else
	return;

    XbaeMatrixEventToXY(w, cbs->event, &x, &y);

    sprintf(buf, "Selected cell %d,%d : %d, %d : event %d, %d",
	    cbs->row, cbs->column, x, y,
	    cbs->event->xbutton.x,
	    cbs->event->xbutton.y);

    if( cbs->row >= 0 && cbs->column >= 0 )
    {
	XbaeMatrixDeselectAll( w );
	XbaeMatrixSelectColumn( w, cbs->column );
	XbaeMatrixSelectRow( w, cbs->row );
    }

    XmTextSetString(text, buf);
}

int
main(argc, argv)
int argc;
char *argv[];
{
    Widget toplevel, form, rc, mw, text;
    XtAppContext app;
#ifdef USE_EDITRES
    extern void _XEditResCheckMessages();
#endif

    toplevel = XtVaAppInitialize(&app, "Choice",
				 NULL, 0,
				 &argc, argv,
				 NULL,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    form = XtVaCreateWidget("form", xmFormWidgetClass, toplevel,
			    NULL);

    text = XtVaCreateManagedWidget(
	"text", xmTextFieldWidgetClass, form,
	XmNeditable,		False,
	XmNcursorPositionVisible, False,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNleftOffset,	4,
	XmNbottomAttachment,	XmATTACH_FORM,
	XmNbottomOffset,	4,
	XmNrightAttachment,	XmATTACH_FORM,
	XmNrightOffset,	4,
	NULL);
    
    mw = XtVaCreateManagedWidget(
	"mw", xbaeMatrixWidgetClass, form,
	XmNtopOffset,		4,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNleftOffset,		4,
	XmNbottomWidget,	text,
	XmNbottomAttachment,	XmATTACH_WIDGET,
	XmNbottomOffset,	4,
	XmNrightAttachment,	XmATTACH_OPPOSITE_FORM,
	XmNrightOffset,	4,
	NULL);
    XtOverrideTranslations(mw,
			   XtParseTranslationTable(
			":<Btn1Down>: SelectCell(cell) EditCell(Pointer)"));

    XtAddCallback(mw, XmNselectCellCallback, cbSelect, (XtPointer) text);
    
    rc = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, form,
	XmNtopAttachment,	XmATTACH_FORM,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNadjustLast,		True,
	XmNorientation,	XmHORIZONTAL,
	XmNnumColumns,		2,
	XmNpacking,		XmPACK_TIGHT,
	NULL);

    createGridType(rc, mw);
    createPlacement(rc, mw);
    
    rc = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, form,
	XmNtopWidget,		rc,
	XmNtopAttachment,	XmATTACH_WIDGET,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNrightAttachment,	XmATTACH_FORM,
	XmNadjustLast,		True,
	XmNorientation,	XmHORIZONTAL,
	XmNnumColumns,		1,
	XmNpacking,		XmPACK_COLUMN,
	NULL);

    createVerticalDisplay(rc, mw);
    createHorizontalDisplay(rc, mw);
    createCellShadowType(rc, mw);
    createShadowType(rc, mw);

    rc = XtVaCreateManagedWidget(
	"rc", xmRowColumnWidgetClass, form,
	XmNtopWidget,		rc,
	XmNtopAttachment,	XmATTACH_WIDGET,
	XmNleftAttachment,	XmATTACH_FORM,
	XmNrightAttachment,	XmATTACH_FORM,
	XmNadjustLast,		True,
	XmNorientation,	XmVERTICAL,
	XmNnumColumns,		4,
	XmNpacking,		XmPACK_COLUMN,
	NULL);
    
    createCellShadowThickness(rc, mw);
    createShadowThickness(rc, mw);
    createSpace(rc, mw);
    createFixedRows(rc, mw);
    createFixedColumns(rc, mw);
    createCellMarginWidth(rc, mw);
    createTrailingFixedRows(rc, mw);
    createTrailingFixedColumns(rc, mw);
    createCellMarginHeight(rc, mw);
    createFill(rc, mw);
    createColumnResize(rc, mw);
    createButtonLabels(rc, mw);

    LoadMatrix(mw);

    XtVaSetValues(mw,
	XmNtopWidget,		rc,
	XmNtopAttachment,	XmATTACH_WIDGET,
	XmNrightWidget,		rc,
	XmNrightAttachment,	XmATTACH_OPPOSITE_WIDGET,
	NULL);
		  
    XtManageChild(form);
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    /*NOTREACHED*/
    return 0;
}


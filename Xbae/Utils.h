/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 * Copyright(c) 1995-99 Andrew Lister
 * Copyright (c) 1999-2002 by the LessTif Developers.
 *
 *                        All rights reserved
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id$
 */

/*
 * Utils.h created by Andrew Lister (6 August, 1995)
 */
#ifndef _Xbae_Utils_h
#define _Xbae_Utils_h

#include <X11/Intrinsic.h>
#include <Xbae/MatrixP.h>

#include <Xbae/Macros.h>


#ifdef __cplusplus
extern "C" {
#endif

void xbaeGetVisibleRows(XbaeMatrixWidget, int *, int *);
void xbaeGetVisibleColumns(XbaeMatrixWidget, int *, int *);
void xbaeGetVisibleCells(XbaeMatrixWidget mw, int *, int *, int *,
			    int *);
void xbaeClearCell(XbaeMatrixWidget, int, int);
void xbaeMakeRowVisible(XbaeMatrixWidget, int);
void xbaeMakeColumnVisible(XbaeMatrixWidget, int);
void xbaeMakeCellVisible(XbaeMatrixWidget, int, int);
void xbaeAdjustTopRow(XbaeMatrixWidget);
void xbaeAdjustLeftColumn(XbaeMatrixWidget);
Boolean xbaeIsRowVisible(XbaeMatrixWidget, int);
Boolean xbaeIsColumnVisible(XbaeMatrixWidget, int);
Boolean xbaeIsCellVisible(XbaeMatrixWidget, int, int);
void xbaeSetClipMask(XbaeMatrixWidget, unsigned int);
void xbaeGetCellTotalWidth(XbaeMatrixWidget);
void xbaeGetCellTotalHeight(XbaeMatrixWidget);
void xbaeGetColumnPositions(XbaeMatrixWidget);
void xbaeGetRowPositions(XbaeMatrixWidget);
void xbaeComputeSize(XbaeMatrixWidget, Boolean, Boolean);
short xbaeMaxRowLabel(XbaeMatrixWidget);
void xbaeParseColumnLabel(String, ColumnLabelLines);
Boolean xbaeEventToXY(XbaeMatrixWidget, XEvent *, int *, int *,
			 CellType *);
Boolean xbaeXYToRowCol(XbaeMatrixWidget, int *, int *, int *, int *,
			  CellType);
int xbaeXtoCol(XbaeMatrixWidget, int);
int xbaeYtoRow(XbaeMatrixWidget, int);
int xbaeXtoTrailingCol(XbaeMatrixWidget, int);
void xbaeRowColToXY(XbaeMatrixWidget, int, int, int *, int *);
int xbaeRowToY(XbaeMatrixWidget, int);
Window xbaeGetCellWindow(XbaeMatrixWidget, Widget *, int, int);
void xbaeCalcVertFill(XbaeMatrixWidget, Window, int, int, int, int,
			 int *, int *, int *, int *);
void xbaeCalcHorizFill(XbaeMatrixWidget, Window, int, int, int, int,
			  int *, int *, int *, int *);
void xbaeRowColToWidgetXY(XbaeMatrixWidget mw, int row, int column,
			    int *widget_x, int *widget_y);
void xbaeMoveWindowFromUserWidgetXY (XbaeMatrixWidget mw,
				     Widget user_widget,
				     int row, int column,
				     int widget_x, int widget_y);

void xbaeObjectLock(Widget);
void xbaeObjectUnlock(Widget);

int _xbaeStrcasecmp(const char *s1, const char *s2);
int _xbaeStrncasecmp(const char *s1, const char *s2, size_t count);

void xbaeScrollRows(XbaeMatrixWidget, Boolean Left, int step);
void xbaeScrollColumns(XbaeMatrixWidget, Boolean Up, int step);

#ifdef __cplusplus
}
#endif

#endif /* _Xbae_Utils_h */

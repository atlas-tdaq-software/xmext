/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 * Copyright(c) 1995-99 Andrew Lister
 * Copyright � 1999, 2000, 2001, 2002 by the LessTif Developers.
 *
 *                        All rights reserved
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * MatrixWidget Author: Andrew Wason, Bellcore, aw@bae.bellcore.com
 *
 * $Id$
 */

/*
 * Create.c created by Andrew Lister (28 Jan, 1996)
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdlib.h>

#include <Xm/Xm.h>
#include <Xm/ScrollBar.h>

#include <Xbae/MatrixP.h>
#include <Xbae/Macros.h>
#include <Xbae/Utils.h>
#include <Xbae/Actions.h>
#include <Xbae/Create.h>

#include <XbaeDebug.h>


static Pixmap createInsensitivePixmap(XbaeMatrixWidget mw);

void
xbaeCopyBackground(Widget widget, int offset, XrmValue *value)
{
    value->addr = (XtPointer)&(widget->core.background_pixel);
}


void
xbaeCopyForeground(Widget widget, int offset, XrmValue *value)
{
    value->addr = (XtPointer)&(((XmManagerWidget)widget)->manager.foreground);
}

void
xbaeCopyDoubleClick(Widget widget, int offset, XrmValue *value)
{
    static int interval;
  
    interval = XtGetMultiClickTime(XtDisplay(widget));
    value->addr = (XtPointer)&interval;
}

void
xbaeCopyCells(XbaeMatrixWidget mw)
{
    String **copy = NULL;
    int i, j;
    Boolean empty_row;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows && mw->matrix.columns)
    {
	/*
	 * Malloc an array of row pointers
	 */
	copy = (String **) XtMalloc(mw->matrix.rows * sizeof(String *));

	/*
	 * Malloc an array of Strings for each row pointer
	 */
	for (i = 0; i < mw->matrix.rows; i++)
	    copy[i] = (String *) XtMalloc(mw->matrix.columns * sizeof(String));

	/*
	 * Create a bunch of "" cells if cells was NULL
	 */
	if (!mw->matrix.cells)
	{
	    for (i = 0; i < mw->matrix.rows; i++)
		for (j = 0; j < mw->matrix.columns; j++)
		    copy[i][j] = XtNewString("");
	}

	/*
	 * Otherwise copy the table passed in
	 */
	else
	{
	    for (i = 0, empty_row = False; i < mw->matrix.rows; i++)
	    {
		if (!empty_row && !mw->matrix.cells[i])
		    empty_row = True;
		for (j = 0; j < mw->matrix.columns; j++)
		{
		    if (empty_row || !mw->matrix.cells[i][j])
		    {
			XtAppWarningMsg(
			    XtWidgetToApplicationContext((Widget)mw),
			    "copyCells", "badValue", "XbaeMatrix",
			    "XbaeMatrix: NULL entry found in cell table",
			    NULL, 0);
			for (;j < mw->matrix.columns; j++)
			    copy[i][j] = XtNewString("");
		    }
		    else
			copy[i][j] = XtNewString(mw->matrix.cells[i][j]);
		}
	    }
	}
    }
    mw->matrix.cells = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyCellWidgets(XbaeMatrixWidget mw)
{
    Widget **copy = NULL;
    int i, j;

    xbaeObjectLock((Widget)mw);

    /*
     * Malloc an array of row pointers
     */
    if (mw->matrix.rows && mw->matrix.columns)
    {
	copy = (Widget **) XtCalloc((Cardinal)mw->matrix.rows,
				    sizeof(Widget *));

	for (i = 0; i < mw->matrix.rows; i++)
	{
	    copy[i] = (Widget *) XtCalloc((Cardinal)mw->matrix.columns,
					  sizeof(Widget));
	    if (mw->matrix.cell_widgets)
		for (j = 0; j < mw->matrix.columns; j++)
		    if (mw->matrix.cell_widgets[i][j])
			copy[i][j] = mw->matrix.cell_widgets[i][j];
	}
    }
    mw->matrix.cell_widgets = copy;
    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyCellShadowTypes(XbaeMatrixWidget mw)
{
    unsigned char **copy = NULL;
    int i, j;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows && mw->matrix.columns)
    {
	copy = (unsigned char **) XtMalloc(mw->matrix.rows *
					   sizeof(unsigned char*));

	for (i = 0; i < mw->matrix.rows; i++)
	    copy[i] = (unsigned char*) XtMalloc(mw->matrix.columns *
						sizeof(unsigned char));

	for (i = 0; i < mw->matrix.rows; i++)
	    for (j = 0; j < mw->matrix.columns; j++)
	    {
		if (!mw->matrix.cell_shadow_types[i][j])

		{
		    XtAppWarningMsg(
			XtWidgetToApplicationContext((Widget) mw),
			"xbaeCopyCellShadowTypes", "badValue", "XbaeMatrix",
			"XbaeMatrix: NULL entry found in cellShadowTypes array",
			NULL, 0);
		    copy[i][j] = XmSHADOW_OUT;
		}
		else
		    copy[i][j] = mw->matrix.cell_shadow_types[i][j];
	    }
    }
    mw->matrix.cell_shadow_types = copy;
    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyRowShadowTypes(XbaeMatrixWidget mw)
{
    unsigned char *copy = NULL;
    int i;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows)
    {
	copy = (unsigned char *) XtMalloc(mw->matrix.rows *
					  sizeof(unsigned char));

	for (i = 0; i < mw->matrix.rows; i++)
	    if (!mw->matrix.row_shadow_types[i])
	    {
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "xbaeCopyRowShadowTypes", "badValue", "XbaeMatrix",
		    "XbaeMatrix: NULL entry found in rowShadowTypes array",
		    NULL, 0);
		copy[i] = XmSHADOW_OUT;
	    }
	    else
		copy[i] = mw->matrix.row_shadow_types[i];
    }
    mw->matrix.row_shadow_types = copy;
    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnShadowTypes(XbaeMatrixWidget mw)
{
    unsigned char *copy = NULL;
    int i;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
	copy = (unsigned char *) XtMalloc(mw->matrix.columns *
					  sizeof(unsigned char));

	for (i = 0; i < mw->matrix.columns; i++)
	    if (!mw->matrix.column_shadow_types[i])
	    {
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "xbaeCopyColumnShadowTypes", "badValue", "XbaeMatrix",
		    "XbaeMatrix: NULL entry found in columnShadowTypes array",
		    NULL, 0);
		copy[i] = XmSHADOW_OUT;
	    }
	    else
		copy[i] = mw->matrix.column_shadow_types[i];
    }
    mw->matrix.column_shadow_types = copy;
    xbaeObjectUnlock((Widget)mw);
}


void
xbaeCopyCellUserData(XbaeMatrixWidget mw)
{
    XtPointer **copy = NULL;
    int i, j;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows && mw->matrix.columns)
    {
	copy = (XtPointer **) XtMalloc(mw->matrix.rows * sizeof(XtPointer*));

	for (i = 0; i < mw->matrix.rows; i++)
	    copy[i] = (XtPointer*) XtMalloc(mw->matrix.columns *
					    sizeof(XtPointer));

	for (i = 0; i < mw->matrix.rows; i++)
	    for (j = 0; j < mw->matrix.columns; j++)
		copy[i][j] = mw->matrix.cell_user_data[i][j];
    }
    mw->matrix.cell_user_data = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyRowUserData(XbaeMatrixWidget mw)
{
    XtPointer *copy = NULL;
    int i;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows)
    {
	copy = (XtPointer *) XtMalloc(mw->matrix.rows * sizeof(XtPointer));

	for (i = 0; i < mw->matrix.rows; i++)
	    copy[i] = mw->matrix.row_user_data[i];
    }
    mw->matrix.row_user_data = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnUserData(XbaeMatrixWidget mw)
{
    XtPointer *copy = NULL;
    int i;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
	copy = (XtPointer *) XtMalloc(mw->matrix.columns * sizeof(XtPointer));

	for (i = 0; i < mw->matrix.columns; i++)
	    copy[i] = mw->matrix.column_user_data[i];
    }
    mw->matrix.column_user_data = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyRowLabels(XbaeMatrixWidget mw)
{
    String *copy = NULL;
    int i;
    Boolean empty_label;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows)
    {
	copy = (String *) XtMalloc(mw->matrix.rows * sizeof(String));

	for (i = 0, empty_label = False; i < mw->matrix.rows; i++)
	    if (empty_label || !mw->matrix.row_labels[i])
	    {
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "copyRowLabels", "badValue", "XbaeMatrix",
		    "XbaeMatrix: NULL entry found in rowLabels array",
		    NULL, 0);
		copy[i] = XtNewString("");
		empty_label = True;
	    }
	    else
		copy[i] = XtNewString(mw->matrix.row_labels[i]);
    }
    mw->matrix.row_labels = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnLabels(XbaeMatrixWidget mw)
{
    String *copy = NULL;
    int i;
    Boolean empty_column;
    
    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
	copy = (String *) XtMalloc(mw->matrix.columns * sizeof(String));

	mw->matrix.column_label_lines = (ColumnLabelLines)
	    XtMalloc(mw->matrix.columns * sizeof(ColumnLabelLinesRec));

	for (i = 0, empty_column = False; i < mw->matrix.columns; i++)
	    if (empty_column || !mw->matrix.column_labels[i])
	    {
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "copyColumnLabels", "badValue", "XbaeMatrix",
		    "XbaeMatrix: NULL entry found in columnLabels array",
		    NULL, 0);
		copy[i] = XtNewString("");
		empty_column = True;
		xbaeParseColumnLabel(
		    copy[i], &mw->matrix.column_label_lines[i]);
	    }
	    else
	    {
		copy[i] = XtNewString(mw->matrix.column_labels[i]);
		xbaeParseColumnLabel(mw->matrix.column_labels[i],
				     &mw->matrix.column_label_lines[i]);
	    }

	/*
	 * Determine max number of lines in column labels
	 */
	mw->matrix.column_label_maxlines =
	    mw->matrix.column_label_lines[0].lines;

	for (i = 1; i < mw->matrix.columns; i++)
	    if (mw->matrix.column_label_lines[i].lines >
		mw->matrix.column_label_maxlines)
		mw->matrix.column_label_maxlines =
		    mw->matrix.column_label_lines[i].lines;
    }
    mw->matrix.column_labels = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnWidths(XbaeMatrixWidget mw)
{
    short *copy = NULL;
    int i;
    Boolean bad = False;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
	copy = (short *) XtMalloc(mw->matrix.columns * sizeof(short));

	for (i = 0; i < mw->matrix.columns; i++)
	{
	    if (!bad && mw->matrix.column_widths[i] == BAD_WIDTH)
	    {
		bad = True;
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "copyColumnWidths", "tooShort", "XbaeMatrix",
		    "XbaeMatrix: Column widths array is too short",
		    NULL, 0);
		copy[i] = 1;
	    }
	    else if (bad)
		copy[i] = 1;
	    else
		copy[i] = mw->matrix.column_widths[i];
	}
    }
    mw->matrix.column_widths = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyRowHeights(XbaeMatrixWidget mw)
{
    short *copy = NULL;
    int i;
    Boolean bad = False;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows)
    {
	copy = (short *) XtMalloc(mw->matrix.rows * sizeof(short));

	for (i = 0; i < mw->matrix.rows; i++)
	{
	    if (!bad && mw->matrix.row_heights[i] == BAD_HEIGHT)
	    {
		bad = True;
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "copyRowHeights", "tooShort", "XbaeMatrix",
		    "XbaeMatrix: Row heights array is too short",
		    NULL, 0);
		copy[i] = 1;
	    }
	    else if (bad)
		copy[i] = 1;
	    else
		copy[i] = mw->matrix.row_heights[i];
	}
    }
    mw->matrix.row_heights = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnMaxLengths(XbaeMatrixWidget mw)
{
    int *copy = NULL;
    int i;
    Boolean bad = False;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
	copy = (int *) XtMalloc(mw->matrix.columns * sizeof(int));

	for (i = 0; i < mw->matrix.columns; i++)
	{
	    if (!bad && mw->matrix.column_max_lengths[i] == BAD_MAXLENGTH)
	    {
		bad = True;
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "copyColumnMaxLengths", "tooShort", "XbaeMatrix",
		    "XbaeMatrix: Column max lengths array is too short",
		    NULL, 0);
		copy[i] = 1;
	    }
	    else if (bad)
		copy[i] = 1;
	    else
		copy[i] = mw->matrix.column_max_lengths[i];
	}
    }
    mw->matrix.column_max_lengths = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnAlignments(XbaeMatrixWidget mw)
{
    unsigned char *copy = NULL;
    int i;
    Boolean bad = False;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
	copy = (unsigned char *) XtMalloc(mw->matrix.columns *
					  sizeof(unsigned char));

	for (i = 0; i < mw->matrix.columns; i++)
	{
	    if (!bad && mw->matrix.column_alignments[i] == BAD_ALIGNMENT)
	    {
		bad = True;
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "copyColumnAlignments", "tooShort", "XbaeMatrix",
		    "XbaeMatrix: Column alignments array is too short",
		    NULL, 0);
		copy[i] = XmALIGNMENT_BEGINNING;
	    }
	    else if (bad)
		copy[i] = XmALIGNMENT_BEGINNING;
	    else
		copy[i] = mw->matrix.column_alignments[i];
	}
    }
    mw->matrix.column_alignments = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnButtonLabels(XbaeMatrixWidget mw)
{
    Boolean *copy = NULL;
    int i;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
	copy = (Boolean *) XtMalloc(mw->matrix.columns *
				    sizeof(Boolean));

	for (i = 0; i < mw->matrix.columns; i++)
	{
	    copy[i] = mw->matrix.column_button_labels[i];
	}
    }
    mw->matrix.column_button_labels = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnFontBold(XbaeMatrixWidget mw)
{
    Boolean *copy = NULL;
    int i;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
      copy = (Boolean *) XtMalloc(mw->matrix.columns *
                                  sizeof(Boolean));

      for (i = 0; i < mw->matrix.columns; i++)
      {
          copy[i] = mw->matrix.column_font_bold[i];
      }
    }
    mw->matrix.column_font_bold = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyRowButtonLabels(XbaeMatrixWidget mw)
{
    Boolean *copy = NULL;
    int i;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows)
    {
	copy = (Boolean *) XtMalloc(mw->matrix.rows *
				    sizeof(Boolean));

	for (i = 0; i < mw->matrix.rows; i++)
	{
	    copy[i] = mw->matrix.row_button_labels[i];
	}
    }
    mw->matrix.row_button_labels = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColumnLabelAlignments(XbaeMatrixWidget mw)
{
    unsigned char *copy = NULL;
    int i;
    Boolean bad = False;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.columns)
    {
	copy = (unsigned char *) XtMalloc(mw->matrix.columns *
					  sizeof(unsigned char));

	for (i = 0; i < mw->matrix.columns; i++)
	{
	    if (!bad &&
		mw->matrix.column_label_alignments[i] == BAD_ALIGNMENT)
	    {
		bad = True;
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget) mw),
		    "copyColumnLabelAlignments", "tooShort",
		    "XbaeMatrix",
		    "XbaeMatrix: Column label alignments array is too short",
		    NULL, 0);
		copy[i] = XmALIGNMENT_BEGINNING;
	    }
	    else if (bad)
		copy[i] = XmALIGNMENT_BEGINNING;
	    else
		copy[i] = mw->matrix.column_label_alignments[i];
	}
    }
    mw->matrix.column_label_alignments = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyColors(XbaeMatrixWidget mw)
{
    Pixel **copy = NULL;
    int i, j;
    Boolean badrow = False;
    Boolean badcol;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows && mw->matrix.columns)
    {
	/*
	 * Malloc an array of row pointers
	 */
	copy = (Pixel **) XtMalloc(mw->matrix.rows * sizeof(Pixel *));

	/*
	 * Malloc an array of Pixels for each row pointer
	 */
	for (i = 0; i < mw->matrix.rows; i++)
	    copy[i] = (Pixel *) XtMalloc(mw->matrix.columns * sizeof(Pixel));

	if (!mw->matrix.colors)
	{
	    for (i = 0; i < mw->matrix.rows; i++)
		for (j = 0; j < mw->matrix.columns; j++)
		    copy[i][j] = mw->manager.foreground;
	}
	else 	for (i = 0; i < mw->matrix.rows; i++)
	{
	    if (!badrow && !mw->matrix.colors[i]) {
		badrow = True;
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget)mw),
		    "copyCellColors", "tooShort",
		    "XbaeMatrix",
		    "XbaeMatrix: Cell ColorPixelTable is too short",
		    NULL, 0);
	    }
	    badcol = badrow;
	    for (j = 0; j < mw->matrix.columns; j++)
	    {
		if (badcol || mw->matrix.colors[i][j] == BAD_PIXEL)
		{
		    badcol = True;
		    if (j > 0)
			copy[i][j] = copy[i][j-1] ;
		    else if (i > 0)
			copy[i][j] = copy[i-1][j] ;
		    else
			copy[i][j] = mw->manager.foreground;
		}
		else
		{
		    copy[i][j] = mw->matrix.colors[i][j];
		}
	    }
	}
    }
    mw->matrix.colors = copy;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCopyBackgrounds(XbaeMatrixWidget mw)
{
    Pixel **copy = NULL;
    int i, j;
    Boolean badrow = False;
    Boolean badcol;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows && mw->matrix.columns)
    {
	/*
	 * Malloc an array of row pointers
	 */
	copy = (Pixel **) XtMalloc(mw->matrix.rows * sizeof(Pixel *));

	/*
	 * Malloc an array of Pixels for each row pointer
	 */
	for (i = 0; i < mw->matrix.rows; i++)
	    copy[i] = (Pixel *) XtMalloc(mw->matrix.columns * sizeof(Pixel));

	if (!mw->matrix.cell_background)
	{
	    for (i = 0; i < mw->matrix.rows; i++)
	    {
		Boolean alt = (mw->matrix.alt_row_count &&
			       i >= (int)mw->matrix.fixed_rows) ? (
				   ((i - (int)mw->matrix.fixed_rows) /
				    mw->matrix.alt_row_count) % 2) : False;

		/*
		 * Assign the even and odd row colours appropriately.  These
		 * will be a copy of the core->background if they have not
		 * been explicitly set but if they have, we want to
		 * preserve the colours as they appear now
		 */
		for (j = 0; j < mw->matrix.columns; j++)
		    copy[i][j] = (alt ? mw->matrix.odd_row_background :
				  mw->matrix.even_row_background);
	    }
	}
	else for (i = 0; i < mw->matrix.rows; i++)
	{
	    if (!badrow && !mw->matrix.cell_background[i]) {
		badrow = True;
		XtAppWarningMsg(
		    XtWidgetToApplicationContext((Widget)mw),
		    "copyCellColors", "tooShort",
		    "XbaeMatrix",
		    "XbaeMatrix: Cell BackgroundPixelTable is too short",
		    NULL, 0);
	    }
	    badcol = badrow;
	    for (j = 0; j < mw->matrix.columns; j++)
	    {
		if (badcol || mw->matrix.cell_background[i][j] == BAD_PIXEL)
		{
		    badcol = True;
		    if (j > 0)
			copy[i][j] = copy[i][j-1] ;
		    else if (i > 0)
			copy[i][j] = copy[i-1][j] ;
		    else
			copy[i][j] = mw->core.background_pixel;
		}
		else
		{
		    copy[i][j] = mw->matrix.cell_background[i][j];
		}
	    }
	}
    }
    mw->matrix.cell_background = copy;

    xbaeObjectUnlock((Widget)mw);
}

/*
 * Copy the selectedCells resource. Create a 2D array of Booleans to
 * represent selected cells if it is NULL.
 */
void
xbaeCopySelectedCells(XbaeMatrixWidget mw)
{
    Boolean **copy = NULL;
    int i, j;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows && mw->matrix.columns)
    {
	/*
	 * Malloc an array of row pointers
	 */
	mw->matrix.num_selected_cells = 0;
#if 0
	copy = (Boolean **) XtMalloc(mw->matrix.rows * sizeof(Boolean *));
#else
	copy = (Boolean **) XtCalloc(mw->matrix.rows, sizeof(Boolean *));
#endif

	/*
	 * Malloc an array of Booleans for each row pointer
	 */
	for (i = 0; i < mw->matrix.rows; i++)
	    copy[i] = (Boolean *) XtCalloc(mw->matrix.columns,
					   sizeof(Boolean));

	/*
	 * If selected_cells is not NULL, copy the table passed in
	 */
	if (mw->matrix.selected_cells)
	    for (i = 0; i < mw->matrix.rows; i++)
		for (j = 0; j < mw->matrix.columns; j++)
		{
		    copy[i][j] = mw->matrix.selected_cells[i][j];
		    if (mw->matrix.selected_cells[i][j])
			mw->matrix.num_selected_cells++;
		}
    }
    mw->matrix.selected_cells = copy;

    xbaeObjectUnlock((Widget)mw);
}


/*
 * Copy the highlightedCells resource. Create a 2D array of Booleans to
 * represent highlighted cells if it is NULL.
 */
void
xbaeCopyHighlightedCells(XbaeMatrixWidget mw)
{
    unsigned char **copy = NULL;
    int i, j;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows && mw->matrix.columns)
    {
	/*
	 * Malloc an array of row pointers
	 */
	copy = (unsigned char **) XtMalloc(mw->matrix.rows *
					   sizeof(Boolean *));

	/*
	 * Malloc an array of Booleans for each row pointer
	 */
	for (i = 0; i < mw->matrix.rows; i++)
	    copy[i] = (unsigned char *) XtCalloc(mw->matrix.columns,
						 sizeof(Boolean));

	/*
	 * If highlighted_cells is not NULL, copy the table passed in
	 */
	if (mw->matrix.highlighted_cells)
	    for (i = 0; i < mw->matrix.rows; i++)
		for (j = 0; j < mw->matrix.columns; j++)
		    copy[i][j] = mw->matrix.highlighted_cells[i][j];
    }
    mw->matrix.highlighted_cells = copy;

    xbaeObjectUnlock((Widget)mw);
}


/*
 * Create a matrix of Pixels
 */
void
xbaeCreateColors(XbaeMatrixWidget mw)
{
    int i;

    xbaeObjectLock((Widget)mw);

    if (mw->matrix.rows && mw->matrix.columns)
    {
	/*
	 * Malloc an array of row pointers
	 */
	mw->matrix.colors = (Pixel **) XtMalloc(mw->matrix.rows *
						sizeof(Pixel *));

	/*
	 * Malloc an array of Pixels for each row pointer
	 */
	for (i = 0; i < mw->matrix.rows; i++)
	    mw->matrix.colors[i] = (Pixel *) XtMalloc(mw->matrix.columns *
						      sizeof(Pixel));
    }
    else
	mw->matrix.colors = NULL;

    xbaeObjectUnlock((Widget)mw);
}

/*
 * A cache for pixmaps which remembers the screen a pixmap belongs to
 * (see bug #591306) and that permits clearing by screen when that
 * screen gets closed.
 */
static struct pcache {
	Pixmap	pixmap;
	Screen	*scr;
	
} *stipple_cache = NULL;
static int	ncache = 0;	/* Allocated size of the array. */
				/* Empty places have NULL screen */

static Pixmap
PixmapFromCache(Screen *scr)
{
	int	i;

	for (i=0; i<ncache; i++)
		if (scr == stipple_cache[i].scr)
			return stipple_cache[i].pixmap;
	return (Pixmap)0;
}

static void
AddPixmapToCache(Screen *scr, Pixmap p)
{
	int	i, old;

	for (i=0; i<ncache; i++)
		if (stipple_cache[i].scr == 0) {
			stipple_cache[i].scr = scr;
			stipple_cache[i].pixmap = p;
			return;
		}

	/* Allocate more */
	if (ncache) {
		old = ncache;
		ncache *= 2;
		stipple_cache = (struct pcache *)XtRealloc((char *)stipple_cache,
			ncache * sizeof(struct pcache));
		for (i=old; i<ncache; i++)
			stipple_cache[i].scr = NULL;
		stipple_cache[old].scr = scr;
		stipple_cache[old].pixmap = p;
	} else {
		ncache = 16;	/* Some silly initial value */
		stipple_cache = (struct pcache *)XtCalloc(ncache,
			sizeof(struct pcache));
		stipple_cache[0].scr = scr;
		stipple_cache[0].pixmap = p;
	}
}

/*
 * Remove the pixmaps with this screen from the cache
 */
static void
RemovePixmapsFromScreen(Screen *scr)
{
	int	i;
	for (i=0; i<ncache; i++)
		if (stipple_cache[i].scr == scr) {
			XFreePixmap(DisplayOfScreen(stipple_cache[i].scr),
				stipple_cache[i].pixmap);
			stipple_cache[i].pixmap = (Pixmap)0;
			stipple_cache[i].scr = NULL;
		}
}

/*
 * Create a pixmap to be used for drawing the matrix contents when
 * XmNsensitive is set to False
 */
static Pixmap
createInsensitivePixmap(XbaeMatrixWidget mw)
{
	static char stippleBits[] = { 0x01, 0x02 };
	Display *dpy = XtDisplay(mw);
	Screen *scr  = XtScreen (mw);
	Pixmap	p;

	xbaeObjectLock((Widget)mw);

	p = PixmapFromCache(XtScreen((Widget)mw));
	if (p) {
		xbaeObjectUnlock((Widget)mw);
		return p;
	}

	p = XCreatePixmapFromBitmapData(dpy, RootWindowOfScreen(scr),
		stippleBits, 2, 2, 0, 1, 1);
	AddPixmapToCache(scr, p);
	xbaeObjectUnlock((Widget)mw);
	return p;
}
    
void
xbaeCreateGridLineGC(XbaeMatrixWidget mw)
{
    XGCValues values;
    XtGCMask mask = GCForeground | GCBackground;

    xbaeObjectLock((Widget)mw);

    values.foreground = mw->matrix.grid_line_color;
    values.background = mw->manager.foreground;
    
    /*
     * GC for drawing grid lines
     */
    mw->matrix.grid_line_gc = XtGetGC((Widget) mw, mask, &values);
    
    /*
     * GC for drawing grid lines with clipping
     */
    mw->matrix.cell_grid_line_gc = XCreateGC(XtDisplay(mw),
					     GC_PARENT_WINDOW(mw),
					     mask, &values);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCreateDrawGC(XbaeMatrixWidget mw)
{
    XGCValues values;
    unsigned long mask = GCForeground | GCFont | GCStipple;

    xbaeObjectLock((Widget)mw);

    /*
     * GC for drawing cells. We create it instead of using a cached one,
     * since the foreground may change frequently.
     */
    values.foreground = mw->manager.foreground;
    values.font = mw->matrix.fid;
    values.stipple = createInsensitivePixmap(mw);

    DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	"xbaeCreateDrawGC(dpy %p win %p fg %d font %p stip %p)\n",
	XtDisplay(mw),
	GC_PARENT_WINDOW(mw),
	values.foreground,
	values.font,
	values.stipple));

    mw->matrix.draw_gc = XCreateGC(XtDisplay(mw),
				   GC_PARENT_WINDOW(mw),
				   mask, &values);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCreatePixmapGC(XbaeMatrixWidget mw)
{
    XGCValues values;
    unsigned long mask = GCForeground | GCGraphicsExposures | GCStipple;

    xbaeObjectLock((Widget)mw);

    values.foreground = mw->manager.foreground;
    values.graphics_exposures = False;
    values.stipple = createInsensitivePixmap(mw);

    mw->matrix.pixmap_gc = XCreateGC(XtDisplay(mw),
				     GC_PARENT_WINDOW(mw),
				     mask, &values);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCreateLabelGC(XbaeMatrixWidget mw)
{
    XGCValues values;
    unsigned long mask = GCForeground | GCFont | GCStipple;

    xbaeObjectLock((Widget)mw);

    /*
     * GC for drawing labels
     */
    values.foreground = mw->manager.foreground;
    values.font = mw->matrix.label_fid;
    values.stipple = createInsensitivePixmap(mw);
    mw->matrix.label_gc = XCreateGC(XtDisplay(mw),
				    GC_PARENT_WINDOW(mw),
				    mask, &values);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCreateLabelClipGC(XbaeMatrixWidget mw)
{
    XGCValues values;
    unsigned long mask = GCForeground | GCFont | GCStipple;

    xbaeObjectLock((Widget)mw);

    /*
     * GC for drawing labels with clipping.
     */
    values.foreground = mw->manager.foreground;
    values.font = mw->matrix.label_fid;
    values.stipple = createInsensitivePixmap(mw);
    mw->matrix.label_clip_gc = XCreateGC(XtDisplay(mw),
					 GC_PARENT_WINDOW(mw),
					 mask, &values);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCreateTopShadowClipGC(XbaeMatrixWidget mw)
{
    XGCValues values;
    XtGCMask mask = GCForeground | GCBackground;

    xbaeObjectLock((Widget)mw);

    /*
     * GC for drawing top shadow inside cells with clipping.
     */
    values.foreground = mw->manager.top_shadow_color;
    values.background = mw->manager.foreground;

    if (mw->manager.top_shadow_pixmap != XmUNSPECIFIED_PIXMAP)
    {
	mask |= GCFillStyle | GCTile;
	values.fill_style = FillTiled;
	values.tile = mw->manager.top_shadow_pixmap;
    }
    mw->matrix.cell_top_shadow_clip_gc = XCreateGC(
	XtDisplay(mw), GC_PARENT_WINDOW(mw), mask, &values);

    mask |= GCFunction;
    values.function = GXxor;
    mw->matrix.resize_top_shadow_gc = XtGetGC(
	(Widget) mw, mask, &values);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeCreateBottomShadowClipGC(XbaeMatrixWidget mw)
{
    XGCValues values;
    XtGCMask mask = GCForeground | GCBackground;

    xbaeObjectLock((Widget)mw);

    /*
     * GC for drawing bottom shadow inside cells with clipping.
     */
    values.foreground = mw->manager.bottom_shadow_color;
    values.background = mw->manager.foreground;

    if (mw->manager.bottom_shadow_pixmap != XmUNSPECIFIED_PIXMAP)
    {
	mask |= GCFillStyle | GCTile;
	values.fill_style = FillTiled;
	values.tile = mw->manager.bottom_shadow_pixmap;
    }
    mw->matrix.cell_bottom_shadow_clip_gc = XCreateGC(
	XtDisplay(mw), GC_PARENT_WINDOW(mw), mask, &values);

    mask |= GCFunction;
    values.function = GXxor;
    mw->matrix.resize_bottom_shadow_gc = XtGetGC(
	(Widget) mw, mask, &values);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeNewFont(XbaeMatrixWidget mw)
{
    XmFontContext context;
    XFontStruct *font;
    XmFontListEntry font_list_entry;
    XmFontType type;
    XFontSetExtents *extents;
    XFontStruct **fonts;
    char **font_names;

    xbaeObjectLock((Widget)mw);

    /*
     * Make a private copy of the FontList
     */
    mw->matrix.font_list = XmFontListCopy(mw->matrix.font_list);

    /*
     * Get XmFontListEntry from FontList
     */
    if (!XmFontListInitFontContext(&context, mw->matrix.font_list))
	XtAppErrorMsg(
	    XtWidgetToApplicationContext((Widget) mw),
	    "newFont", "badFont", "XbaeMatrix",
	    "XbaeMatrix: XmFontListInitFontContext failed, bad fontList",
	    NULL, 0);

    if ((font_list_entry = XmFontListNextEntry(context)) == NULL)
	XtAppErrorMsg(
	    XtWidgetToApplicationContext((Widget) mw),
	    "newFont", "badFont", "XbaeMatrix",
	    "XbaeMatrix: XmFontListNextEntry failed, no next fontList",
	    NULL, 0);

    font = (XFontStruct*)XmFontListEntryGetFont(font_list_entry, &type);

    if (type == XmFONT_IS_FONTSET)
    {
	mw->matrix.font_set = (XFontSet)font;
	mw->matrix.font_struct = (XFontStruct*)NULL;

	extents = XExtentsOfFontSet((XFontSet)font);
	mw->matrix.font_width = extents->max_logical_extent.width;
	mw->matrix.font_height = extents->max_logical_extent.height;
	mw->matrix.font_y = extents->max_logical_extent.y;

	XFontsOfFontSet((XFontSet)font, &fonts, &font_names);
	mw->matrix.fid = fonts[0]->fid;
    }
    else
    {
	mw->matrix.font_set = (XFontSet)NULL;
	mw->matrix.font_struct = font;

	mw->matrix.font_width = (font->max_bounds.width + font->min_bounds.width) /2;
	mw->matrix.font_height = (font->max_bounds.descent + font->max_bounds.ascent);
	mw->matrix.font_y = -font->max_bounds.ascent;

	mw->matrix.fid = font->fid;
    }

    XmFontListFreeFontContext(context);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeNewLabelFont(XbaeMatrixWidget mw)
{
    XmFontContext context;
    XFontStruct *font;
    XmFontListEntry font_list_entry;
    XmFontType type;
    XFontSetExtents *extents;
    XFontStruct **fonts;
    char **font_names;

    xbaeObjectLock((Widget)mw);

    /*
     * Make a private copy of the FontList
     */
    mw->matrix.label_font_list = XmFontListCopy(mw->matrix.label_font_list);

    /*
     * Get XmFontListEntry from FontList
     */
    if (!XmFontListInitFontContext(&context, mw->matrix.label_font_list))
	XtAppErrorMsg(
	    XtWidgetToApplicationContext((Widget) mw),
	    "newFont", "badLabelFont", "XbaeMatrix",
	    "XbaeMatrix: XmFontListInitFontContext failed, bad labelFontList",
	    NULL, 0);

    if ((font_list_entry = XmFontListNextEntry(context)) == NULL)
	XtAppErrorMsg(
	    XtWidgetToApplicationContext((Widget) mw),
	    "newFont", "badLabelFont", "XbaeMatrix",
	    "XbaeMatrix: XmFontListNextEntry failed, no next fontList",
	    NULL, 0);

    font = (XFontStruct*)XmFontListEntryGetFont(font_list_entry, &type);

    if (type == XmFONT_IS_FONTSET)
    {
	mw->matrix.label_font_set = (XFontSet)font;
	mw->matrix.label_font_struct = (XFontStruct*)NULL;

	extents = XExtentsOfFontSet((XFontSet)font);
	mw->matrix.label_font_width = extents->max_logical_extent.width;
	mw->matrix.label_font_height = extents->max_logical_extent.height;
	mw->matrix.label_font_y = extents->max_logical_extent.y;

	XFontsOfFontSet((XFontSet)font, &fonts, &font_names);
	mw->matrix.label_fid = fonts[0]->fid;
    }
    else
    {
	mw->matrix.label_font_set = (XFontSet)NULL;
	mw->matrix.label_font_struct = font;

	mw->matrix.label_font_width = (font->max_bounds.width + font->min_bounds.width) /2;
	mw->matrix.label_font_height = (font->max_bounds.descent + font->max_bounds.ascent);
	mw->matrix.label_font_y = -font->max_bounds.ascent;

	mw->matrix.label_fid = font->fid;
    }

    XmFontListFreeFontContext(context);

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeFreeCells(XbaeMatrixWidget mw)
{
    int i, j;

    if (!mw->matrix.cells)
	return;

    xbaeObjectLock((Widget)mw);

    /*
     * Free each cell in a row, then free the row and go to the next one
     */
    for (i = 0; i < mw->matrix.rows; i++)
    {
	for (j = 0; j < mw->matrix.columns; j++)
	    XtFree((char *) mw->matrix.cells[i][j]);
	XtFree((char *) mw->matrix.cells[i]);
    }

    /*
     * Free the array of row pointers
     */
    XtFree((char *) mw->matrix.cells);
    mw->matrix.cells = NULL;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeFreeCellWidgets(XbaeMatrixWidget mw)
{
    int i/*, j*/;

    if (!mw->matrix.cell_widgets)
	return;

    xbaeObjectLock((Widget)mw);

    /*
     * Destroy each cell widget (don't XtFree ! as Linas suggests),
     * then free the row and go to the next one.
     */
    for (i = 0; i < mw->matrix.rows; i++)
    {
/*	If the actual matrix is the parent of the cell widgets, they
	are already destroyed by the XtDestroyWidget call for this 
        matrix. Destroying them explicitely causes double free and
        memory corruption.
        Serguei Kolos 10/06/2013
	for (j = 0; j < mw->matrix.columns; j++)
	    if (mw->matrix.cell_widgets[i][j])
		XtDestroyWidget(mw->matrix.cell_widgets[i][j]); 
*/
	XtFree((char *) mw->matrix.cell_widgets[i]);
    }

    /*
     * Free the array of row pointers
     */
    XtFree((char *) mw->matrix.cell_widgets);
    mw->matrix.cell_widgets = NULL;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeFreeRowLabels(XbaeMatrixWidget mw)
{
    int i;

    if (!mw->matrix.row_labels)
	return;

    xbaeObjectLock((Widget)mw);

    for (i = 0; i < mw->matrix.rows; i++)
	XtFree((char *) mw->matrix.row_labels[i]);

    XtFree((char *) mw->matrix.row_labels);
    mw->matrix.row_labels = NULL;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeFreeColumnLabels(XbaeMatrixWidget mw)
{
    int i;

    if (!mw->matrix.column_labels)
	return;

    xbaeObjectLock((Widget)mw);

    for (i = 0; i < mw->matrix.columns; i++)
    {
	XtFree((char *) mw->matrix.column_labels[i]);
	XtFree((char *) mw->matrix.column_label_lines[i].lengths);
    }

    XtFree((char *) mw->matrix.column_label_lines);
    XtFree((char *) mw->matrix.column_labels);
    mw->matrix.column_labels = NULL;

    xbaeObjectUnlock((Widget)mw);
}


void
xbaeFreeColors(XbaeMatrixWidget mw)
{
    int i;

    if (!mw->matrix.colors)
	return;

    xbaeObjectLock((Widget)mw);

    /*
     * Free each row of Pixels
     */
    for (i = 0; i < mw->matrix.rows; i++)
	XtFree((char *) mw->matrix.colors[i]);

    /*
     * Free the array of row pointers
     */
    XtFree((char *) mw->matrix.colors);
    mw->matrix.colors = NULL;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeFreeBackgrounds(XbaeMatrixWidget mw)
{
    int i;

    if (!mw->matrix.cell_background)
	return;

    xbaeObjectLock((Widget)mw);

    /*
     * Free each row of Pixels
     */
    for (i = 0; i < mw->matrix.rows; i++)
	XtFree((char *) mw->matrix.cell_background[i]);

    /*
     * Free the array of row pointers
     */
    XtFree((char *) mw->matrix.cell_background);
    mw->matrix.cell_background = NULL;

    xbaeObjectUnlock((Widget)mw);
}

void
xbaeFreeSelectedCells(XbaeMatrixWidget mw)
{
    int i;

    /*
     * Free each row of XtPointer pointers
     */
    if (!mw->matrix.selected_cells)
	return;

    xbaeObjectLock((Widget)mw);

    for (i = 0; i < mw->matrix.rows; i++)
	XtFree((char *) mw->matrix.selected_cells[i]);

    /*
     * Free the array of row pointers
     */
    XtFree((char *) mw->matrix.selected_cells);
    mw->matrix.selected_cells = NULL;

    xbaeObjectUnlock((Widget)mw);
}


void
xbaeFreeHighlightedCells(XbaeMatrixWidget mw)
{
    int i;

    if (!mw->matrix.highlighted_cells)
	return;
    
    xbaeObjectLock((Widget)mw);

    /*
     * Free each row of XtPointer pointers
     */
    for (i = 0; i < mw->matrix.rows; i++)
	XtFree((char *) mw->matrix.highlighted_cells[i]);

    /*
     * Free the array of row pointers
     */
    XtFree((char *) mw->matrix.highlighted_cells);
    mw->matrix.highlighted_cells = NULL;

    xbaeObjectUnlock((Widget)mw);
}


void
xbaeFreeCellUserData(XbaeMatrixWidget mw)
{
    xbaeObjectLock((Widget)mw);

    if (mw->matrix.cell_user_data)
    {
	int i;

	/*
	 * Free each row of Booleans
	 */
	for (i = 0; i < mw->matrix.rows; i++)
	    XtFree((char *) mw->matrix.cell_user_data[i]);

	/*
	 * Free the array of row pointers
	 */
	XtFree((char *) mw->matrix.cell_user_data);
    }
    mw->matrix.cell_user_data = NULL;

    xbaeObjectUnlock((Widget)mw);
}


void
xbaeFreeCellShadowTypes(XbaeMatrixWidget mw)
{
    xbaeObjectLock((Widget)mw);

    if (mw->matrix.cell_shadow_types)
    {
	int i;

	/*
	 * Free each row of unsigned char pointers
	 */
	for (i = 0; i < mw->matrix.rows; i++)
	    XtFree((char *) mw->matrix.cell_shadow_types[i]);

	/*
	 * Free the array of row pointers
	 */
	XtFree((char *) mw->matrix.cell_shadow_types);
    }
    mw->matrix.cell_shadow_types = NULL;

    xbaeObjectUnlock((Widget)mw);
}

/*
 * Make sure to know when our display connection dies.
 */
#if XtSpecificationRelease >= 6
static void
DisplayDied(Widget w, XtPointer client, XtPointer call)
{
	XtDestroyHookDataRec	*p = (XtDestroyHookDataRec *)call;

	if (p == NULL || p->type != XtHdestroy)
		return;

	if (XtIsSubclass(p->widget, xmPrimitiveWidgetClass))
		RemovePixmapsFromScreen(XtScreen(p->widget));
}
#endif

void
RegisterDisplay(Widget w)
{
#if XtSpecificationRelease >= 6
	Display	*d = XtDisplay(w);
	XtAddCallback(XtHooksOfDisplay(d), XtNdestroyHook, DisplayDied, NULL);
#endif
}

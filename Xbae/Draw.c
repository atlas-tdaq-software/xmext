/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 * Copyright(c) 1995-99 Andrew Lister
 * Copyright � 1999, 2000, 2001, 2002 by the LessTif Developers.
 *
 *                        All rights reserved
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id$
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <Xm/Xm.h>
#include <Xm/XmP.h>
#include <Xm/DrawP.h>
#include <Xbae/MatrixP.h>
#include <Xbae/Utils.h>
#include <Xbae/Shadow.h>
#include <Xbae/Draw.h>

#include <XbaeDebug.h>


static void xbaeDrawCellString(XbaeMatrixWidget, int, int, int, int,
				  String, Pixel, Pixel);
static void xbaeDrawCellWidget(XbaeMatrixWidget, int, int, int, int,
				  Widget, Pixel, Pixel);
static void xbaeDrawCellPixmap(XbaeMatrixWidget, int, int, int, int,
				  Pixmap, Pixmap, int, int, Pixel,
				  Pixel, int);
static void xbaeDrawString(XbaeMatrixWidget mw, Window win, GC gc,
			String string, int length,
			int x, int y,
			int maxlen, int maxht,
			unsigned char alignment, Boolean highlight,
			Boolean bold, Boolean rowLabel,
			Boolean colLabel, Pixel color);

/*
 * Draw a fixed or non-fixed cell. The coordinates are calculated relative
 * to the correct window and pixmap is copied to that window.
 */
static void
xbaeDrawCellPixmap(XbaeMatrixWidget mw,
		int row,
		int column,
		int x,
		int y,
		Pixmap pixmap,
		Pixmap mask,
		int width,
		int height,
		Pixel bg,
		Pixel fg,
		int depth)
{
    int src_x = 0, src_y, dest_x, dest_y;
    int copy_width, copy_height;
    int cell_height = SOME_ROW_HEIGHT(mw, row);
    int cell_width = COLUMN_WIDTH(mw, column);
    Widget w;
    unsigned char alignment = mw->matrix.column_alignments ?
	mw->matrix.column_alignments[column] : XmALIGNMENT_BEGINNING;
    Display *display = XtDisplay(mw);
    GC gc;
    Window win = xbaeGetCellWindow(mw, &w, row, column);

    if (!win)
	return;

    /*
     * Convert the row/column to the coordinates relative to the correct
     * window
     */
    dest_x = x + TEXT_WIDTH_OFFSET(mw);

    gc = mw->matrix.pixmap_gc;

    XSetForeground(display, gc, bg);

    /*
     * If we are only changing the highlighting of a cell, we don't need
     * to do anything other than draw (or undraw) the highlight
     */
    if (mw->matrix.highlighted_cells &&
	mw->matrix.highlight_location != HighlightNone)
    {
	xbaeDrawCellHighlight(mw, win, gc, row, column, x, y, cell_width,
			      cell_height, mw->matrix.highlight_location);
	return;
    }

    XFillRectangle(display, win, gc, x, y,
		   COLUMN_WIDTH(mw, column), cell_height);

    XSetForeground(display, gc, fg);
    XSetBackground(display, gc, bg);

    /*
     * Adjust the x and y drawing destinations as appropriate.  First the
     * y value....
     */
    dest_y = y;
    if (height > cell_height)
    {
	/* Adjust the starting location in the src image */
	src_y = (height - cell_height) / 2;
	copy_height = cell_height;
    }
    else
    {
	/* Adjust the destination point */
	src_y = 0;
	dest_y += ((cell_height - height) / 2);
	copy_height = height;
    }

    /*
     * Adjust the x value, paying attention to the columnAlignment
     */
    if (width > cell_width)
	copy_width = cell_width;
    else
	copy_width = width;

    switch (alignment)
    {
    case XmALIGNMENT_BEGINNING:
	src_x = 0;
	break;
    case XmALIGNMENT_CENTER:
	if (width > cell_width)
	    src_x = (width - cell_width) / 2;
	else
	{
	    src_x = 0;
	    dest_x += ((cell_width - width) / 2);
	}
	break;
    case XmALIGNMENT_END:
	if (width > cell_width)
	    src_x = width - cell_width;
	else
	{
	    src_x = 0;
	    dest_x = x + COLUMN_WIDTH(mw, column) - TEXT_WIDTH_OFFSET(mw) -
		width;
	}
	break;
    }

    /*
     * Draw the pixmap.  Clip it, if necessary
     */
    if (pixmap)
    {
	if (depth > 1)		/* A pixmap using xpm */
	{
	    if (mask)
	    {
		XSetClipMask(display, gc, mask);

		XSetClipOrigin(display, gc, dest_x - src_x, dest_y - src_y);
	    }
	    XCopyArea(display, pixmap, win, gc, src_x, src_y, copy_width,
		      copy_height, dest_x, dest_y);
	    if (mask)
		XSetClipMask(display, gc, None);
	}
	else			/* A plain old bitmap */
	    XCopyPlane(display, pixmap, win, gc, src_x, src_y, copy_width,
		       copy_height, dest_x, dest_y, 1L);
    }
    
    XSetForeground(display, gc, bg);
    
    /*
     * If we need to fill the rest of the space, do so
     */
    if (NEED_VERT_FILL(mw) && (row == (mw->matrix.rows - 1)))
    {
	int ax, ay;
	int fill_width, fill_height;
	/*
	 * Need to check the actual window we are drawing on to ensure
	 * the correct visual
	 */
	xbaeCalcVertFill(mw, win, x, y, row, column, &ax, &ay,
			 &fill_width, &fill_height);

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	  "xbaeDrawCellPixmap[%d,%d]: add vert fill x=%d, y=%d, w=%d, h=%d\n",
	  row, column, ax, ay, fill_width, fill_height));

	XFillRectangle(XtDisplay(mw), XtWindow(mw), gc,
		       ax, ay, fill_width, fill_height);
    }

    if (NEED_HORIZ_FILL(mw) && (column == (mw->matrix.columns - 1)))
    {
	int ax, ay;
	int fill_width, fill_height;

	xbaeCalcHorizFill(mw, win, x, y, row, column, &ax, &ay,
			  &fill_width, &fill_height);

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	  "xbaeDrawCellPixmap[%d,%d]: add horiz fill x=%d, y=%d, w=%d, h=%d\n",
	  row, column, ax, ay, fill_width, fill_height));

	XFillRectangle(XtDisplay(mw), XtWindow(mw), gc,
		       ax, ay, fill_width, fill_height);
    }

    if (NEED_HORIZ_FILL(mw) && NEED_VERT_FILL(mw) &&
      (row == (mw->matrix.rows - 1)) &&
      (column == (mw->matrix.columns - 1)))
      {
      int h_x, h_y, h_w, h_h;
      int v_x, v_y, v_w, v_h;

      xbaeCalcHorizFill(mw, win, x, y, row, column, &h_x, &h_y, &h_w, &h_h);
      xbaeCalcVertFill(mw, win, x, y, row, column, &v_x, &v_y, &v_w, &v_h);

      if (h_y < ROW_LABEL_OFFSET(mw)) {
                h_h = h_y + h_h - ROW_LABEL_OFFSET(mw);
                h_y = ROW_LABEL_OFFSET(mw);
        }

      DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	  "xbaeDrawCellPixmap[%d,%d]: add horiz/vert fill (mod) x=%d, y=%d, w=%d, h=%d\n",
	  row, column, h_x, v_y, h_w, v_h));

      XFillRectangle(XtDisplay(mw), XtWindow(mw), gc,
		       h_x, v_y, h_w, v_h);
      }

    if (mw->matrix.highlighted_cells &&
	mw->matrix.highlighted_cells[row][column])
    {
	xbaeDrawCellHighlight(mw, win, gc, row, column, x, y, cell_width,
			      cell_height, HIGHLIGHTING_SOMETHING);
    }
    xbaeDrawCellShadow(mw, win, row, column, x, y, cell_width,
		       cell_height, False, False, False);
}

/*
 * Draw a fixed or non-fixed cell. The coordinates are calculated relative
 * to the correct window and the cell is drawn in that window.
 */
static void
xbaeDrawCellString(XbaeMatrixWidget mw,
		int row,
		int column,
		int x,
		int y,
		String string,
		Pixel bg,
		Pixel fg)
{
    GC gc;
    Widget w;
    Window win = xbaeGetCellWindow(mw, &w, row, column);
    Dimension column_width = COLUMN_WIDTH(mw, column);
    Dimension row_height;
    Dimension width = column_width;
    Dimension height;
    Boolean selected = mw->matrix.selected_cells ?
	mw->matrix.selected_cells[row][column] : False;
    String str = string;

    if (!win)
	return;

    DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	"xbaeDrawCellString[%d,%d] x %d y %d '%s'\n",
	row, column, x, y, string));

    if (mw->matrix.row_heights)
	row_height = SOME_ROW_HEIGHT(mw, row);
    else
	row_height = ROW_HEIGHT(mw);

    if (row_height == 0)
	return;		/* Quick fix */

    height = row_height;
    gc = mw->matrix.draw_gc;
    XSetForeground(XtDisplay(mw), gc, bg);

    /*
     * If we are only changing the highlighting of a cell, we don't need
     * to do anything other than draw (or undraw) the highlight
     */
    if (mw->matrix.highlighted_cells &&
	mw->matrix.highlight_location != HighlightNone)
    {
	xbaeDrawCellHighlight(mw, win, gc, row, column, x, y, width, height,
			      mw->matrix.highlight_location);
	return;
    }

    /*
     * Fill the cell's background if it can be done
     * without duplicating work below
     */
    if ((XtWindow(mw) != win) ||
	(!(IN_GRID_COLUMN_MODE(mw) && NEED_VERT_FILL(mw) &&
	   ((mw->matrix.rows - 1) == row)) &&
	 !(IN_GRID_ROW_MODE(mw) && NEED_HORIZ_FILL(mw) &&
	   ((mw->matrix.columns - 1) == column))))
	XFillRectangle(XtDisplay(mw), win, gc, x, y,
		       column_width, row_height);

    /*
     * If we need to fill the rest of the space, do so
     */
    if (NEED_VERT_FILL(mw) && ((mw->matrix.rows - 1) == row))
    {
	Pixel cbg;
	int ax, ay;
	int fill_width, fill_height;
	/*
	 * Need to check the actual window we are drawing on to ensure
	 * the correct visual
	 */
	xbaeCalcVertFill(mw, win, x, y, row, column, &ax, &ay,
			 &fill_width, &fill_height);

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	  "xbaeDrawCellPixmap[%d,%d]: add vert fill x=%d, y=%d, w=%d, h=%d\n",
	  row, column, ax, ay, fill_width, fill_height));

        xbaeComputeCellBg(mw, row, column, &cbg);
	XSetForeground(XtDisplay(mw), gc, cbg);
	XFillRectangle(XtDisplay(mw), XtWindow(mw), gc,
		       ax, ay, fill_width, fill_height);
    }

    if (NEED_HORIZ_FILL(mw) && (column == (mw->matrix.columns - 1)))
    {
	Pixel cbg;
	int ax, ay;
	int fill_width, fill_height;

	xbaeCalcHorizFill(mw, win, x, y, row, column, &ax, &ay,
			  &fill_width, &fill_height);
	/*
	 * If we're above the row-label-offset, only start drawing
	 * from the row level highlight so change ay and fill height
	 * accordingly.
	 * xbaeCalcHorizFill() cannot be expected to handle this
	 * as this must also be applied for the highlight.
	 */

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	  "xbaeDrawCellString[%d,%d]: add horiz fill x=%d, y=%d, w=%d, h=%d\n",
	  row, column, ax, ay, fill_width, fill_height));

	if (ay < ROW_LABEL_OFFSET(mw)) {
		fill_height = ay + fill_height - ROW_LABEL_OFFSET(mw);
		ay = ROW_LABEL_OFFSET(mw);
	}

        DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	  "xbaeDrawCellString[%d,%d]: add horiz fill (mod) x=%d, y=%d, w=%d, h=%d\n",
	  row, column, ax, ay, fill_width, fill_height));

        xbaeComputeCellBg(mw, row, column, &cbg);
	XSetForeground(XtDisplay(mw), gc, cbg);
	XFillRectangle(XtDisplay(mw), XtWindow(mw), gc,
		       ax, ay, fill_width, fill_height);
    }

    if (NEED_HORIZ_FILL(mw) && NEED_VERT_FILL(mw) &&
      (row == (mw->matrix.rows - 1)) &&
      (column == (mw->matrix.columns - 1)))
    {
	Pixel cbg;
	int h_x, h_y, h_w, h_h;
	int v_x, v_y, v_w, v_h;

	xbaeCalcHorizFill(mw, win, x, y, row, column, &h_x, &h_y, &h_w, &h_h);
	xbaeCalcVertFill(mw, win, x, y, row, column, &v_x, &v_y, &v_w, &v_h);

	if (h_y < ROW_LABEL_OFFSET(mw)) {
		h_h = h_y + h_h - ROW_LABEL_OFFSET(mw);
		h_y = ROW_LABEL_OFFSET(mw);
	}

	DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
	  "xbaeDrawCellString[%d,%d]: add horiz/vert fill (mod) x=%d, y=%d, w=%d, h=%d\n",
	  row, column, h_x, v_y, h_w, v_h));

	xbaeComputeCellBg(mw, row, column, &cbg);
	XSetForeground(XtDisplay(mw), gc, cbg);
	XFillRectangle(XtDisplay(mw), XtWindow(mw), gc,
		       h_x, v_y, h_w, v_h);
    }

    XSetForeground(XtDisplay(mw), gc, bg);
    /*
     * Draw the string in the cell.
     */
    xbaeDrawString(mw, win, gc, str, strlen(str),
		x + TEXT_X_OFFSET(mw), y + TEXT_Y_OFFSET(mw),
		mw->matrix.column_widths[column],
		row_height,
		mw->matrix.column_alignments ?
		mw->matrix.column_alignments[column] :
		XmALIGNMENT_BEGINNING, selected,
		mw->matrix.column_font_bold ? mw->matrix.column_font_bold[column] : 0,
		False, False, fg);

    if (mw->matrix.highlighted_cells &&
	mw->matrix.highlighted_cells[row][column])
    {
	xbaeDrawCellHighlight(mw, win, gc, row, column, x, y, width, height,
			      HIGHLIGHTING_SOMETHING);
    }

    xbaeDrawCellShadow(mw, win, row, column, x, y, COLUMN_WIDTH(mw, column),
	mw->matrix.row_heights ? SOME_ROW_HEIGHT(mw, row) : ROW_HEIGHT(mw),
	False, False, False);
}

/*
 * Draw a user defined widget in the cell
 */
static void
xbaeDrawCellWidget(XbaeMatrixWidget mw,
		int row,
		int column,
		int x,
		int y,
		Widget widget,
		Pixel bg,
		Pixel fg)
{
    GC gc;
    Widget w;
    Window win = xbaeGetCellWindow(mw, &w, row, column);
	int	widget_x, widget_y;
	int	window_x, window_y;

    Boolean clipped = False;	/* To get 4.7 to compile. */
    int	r = SOME_ROW_HEIGHT(mw, row);

    DEBUGOUT(_XbaeDebug2(__FILE__, (Widget)mw, widget,
	"xbaeDrawCellWidget: rc %d,%d xy %d,%d\n",
	row, column, x, y));

    if (!win)
	return;
    if (! XtIsManaged(widget))
	return;

    gc = mw->matrix.draw_gc;
    XSetForeground(XtDisplay(mw), gc, bg);
    XFillRectangle(XtDisplay(mw), win, gc, x, y, COLUMN_WIDTH(mw, column), r);

    /*
     * Draw the widget in the cell.
     */
    xbaeRowColToWidgetXY (mw, row, column, &widget_x, &widget_y);

    widget_x -= mw->matrix.cell_shadow_thickness +
	mw->matrix.cell_highlight_thickness;

    widget_y -= mw->matrix.cell_shadow_thickness +
	mw->matrix.cell_highlight_thickness;

    window_x = x - mw->matrix.cell_shadow_thickness -
	mw->matrix.cell_highlight_thickness;

    window_y = y - mw->matrix.cell_shadow_thickness -
	mw->matrix.cell_highlight_thickness;

    XtMoveWidget (widget, widget_x, widget_y);

    /* Need to call XMoveWindow after every call to
       XtMoveWidget, to make sure the widget appears in the
       correct place, and Intrinsics recognises the new
       coordinates correctly - Phil Eccles 10/04/2002 */
    XMoveWindow (XtDisplay(mw), XtWindow(widget),
	window_x, window_y);

    xbaeDrawCellShadow(mw, win, row, column, x, y, COLUMN_WIDTH(mw, column),
		       r, False, clipped, False);
}

/*
 * Width in pixels of a character in a given font
 */
#define charWidth(fs,c) ((fs)->per_char ? \
                         (fs)->per_char[((c) < (fs)->min_char_or_byte2 ? \
					 (fs)->default_char : \
					 (c) - \
					 (fs)->min_char_or_byte2)].width : \
			 (fs)->min_bounds.width)


/*
 * Draw a string with specified attributes. We want to avoid having to
 * use a GC clip_mask, so we clip by characters. This complicates the code.
 */
	/* FIX ME this should be clipped with the row height for the last row */
static void
xbaeDrawString(XbaeMatrixWidget mw,
		Window win,
		GC gc,
		String string,
		int length,
		int x,
		int y,
		int maxlen,
		int maxht,		/* Added for row heights */
		unsigned char alignment,
		Boolean highlight,
		Boolean bold,
		Boolean rowLabel,
		Boolean colLabel,
		Pixel color)
{
	int start, width, maxwidth, font_height;
	XFontStruct	*font_struct;
	XFontSet	font_set;
	Boolean choppedStart = False;
	Boolean choppedEnd = False;
	XRectangle *ink_array = NULL;
	XRectangle *logical_array = NULL;
	int num_chars, doclip;
	XRectangle overall_logical, clipht;

	DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
		"xbaeDrawString(%s) x %d y %d, clip w %d h %d\n",
		string, x, y, maxlen, maxht));

	if (rowLabel || colLabel) {
		font_struct = mw->matrix.label_font_struct;
		font_set = mw->matrix.label_font_set;
	} else {
		font_struct = mw->matrix.font_struct;
		font_set = mw->matrix.font_set;
	}

	/*
	 * Initialize starting character in string
	 */
	start = 0;

	if (!rowLabel) {
		maxwidth = maxlen * FONT_WIDTH(mw);
		font_height = FONT_HEIGHT(mw);
	} else {
		maxwidth = maxlen * LABEL_WIDTH(mw);
		font_height = LABEL_HEIGHT(mw);
	}

	/*
	 * Clip height
	 */
	if (maxht < 0) {
		doclip = 0;
	} else {
		doclip = 1;
	}

#if 0
	clipht.height = y + font_height;
	clipht.width = x + maxwidth;
	clipht.x = 0;
	clipht.y = 0;

	if (doclip) {
		DEBUGOUT(_XbaeDebug(__FILE__, mw,
			"XSetClipRectangles - x %d y %d w %d h %d\n",
			x, y, clipht.width, clipht.height));
		XSetClipRectangles(XtDisplay(mw), gc, 0, 0, &clipht, 1, Unsorted);
	}
#endif

    if (font_set)
    {
	ink_array = (XRectangle*)XtMalloc(length * sizeof(XRectangle));
	logical_array = (XRectangle*)XtMalloc(length * sizeof(XRectangle));

	XmbTextPerCharExtents(font_set, string, length,
	    ink_array, logical_array, length, &num_chars,
	    NULL, &overall_logical);

	/*
	 * If the width of the string is greater than the width of this cell,
	 * we need to clip. We don't want to use the server to clip because
	 * it is slow, so we truncate characters if we exceed a cells pixel
	 * width.
	 */
	if (overall_logical.width > maxwidth)
	{
	    switch (alignment)
	    {

	    case XmALIGNMENT_CENTER:
	    {
		int startx;
		int endx;
		int i;
		int end;

		/*
		 * If we are going to draw arrows at both ends, allow for them.
		 */
		if (mw->matrix.show_arrows)
		{
		    maxwidth -= 2 * mw->matrix.font_width;
		    choppedStart = True;
		    choppedEnd = True;
		}

		/*
		 * Find limits of cell relative to the origin of the string.
		 */
		startx = overall_logical.x + overall_logical.width / 2 -
		    maxwidth / 2;
		endx = startx + maxwidth - 1;

		/*
		 * Find the first character which fits into the cell.
		 */
		for (i = 0; i < num_chars && logical_array[i].x < startx; ++i)
		{
		    int cl = mblen(string + start, length);
		    start += cl;
		    length -= cl;
		}

		/*
		 * Find the last character which fits into the cell.
		 * At this point length represents the number of bytes
		 * between the end of the cell and the end of the full
		 * string. Note that the scan continues from above.
		 */
		for (end = start; i < num_chars && (logical_array[i].x +
						    logical_array[i].width) <
			 endx; ++i)
		{
		    int cl = mblen(string + end, length);
		    end += cl;
		    length -= cl;
		}

		/*
		 * Now reset length so that it represents the number of bytes
		 * in the string.
		 */
		length = end - start;

		break;
	    }

	    case XmALIGNMENT_END:
	    {
		int startx;
		int i;

		/*
		 * We are going to an draw arrow at the end, allow for it.
		 */
		if (mw->matrix.show_arrows)
		{
		    maxwidth -= mw->matrix.font_width;
		    choppedEnd = True;
		}

		/*
		 * Find limits of cell relative to the origin of the string.
		 */
		startx = overall_logical.x + overall_logical.width - maxwidth;

		/*
		 * Find the first character which fits into the cell.
		 */
		for (i = 0; i < num_chars && logical_array[i].x < startx; ++i)
		{
		    int cl = mblen(string + start, length);
		    start += cl;
		    length -= cl;
		}

		break;
	    }

	    case XmALIGNMENT_BEGINNING:
	    default:
	    {
		int endx;
		int i;
		int end;

		/*
		 * We are going to an draw arrow at the start, allow for it.
		 */
		if (mw->matrix.show_arrows)
		{
		    maxwidth -= mw->matrix.font_width;
		    choppedStart = True;
		}

		/*
		 * Find limits of cell relative to the origin of the string.
		 */
		endx = overall_logical.x + maxwidth - 1;

		/*
		 * Find the last character which fits into the cell.
		 * At this point length represents the number of bytes
		 * between the end of the cell and the end of the full
		 * string.
		 */
		for (i = 0, end = start;
		     i < num_chars && (logical_array[i].x +
				       logical_array[i].width) < endx; ++i)
		{
		    int cl = mblen(string + end, length);
		    end += cl;
		    length -= cl;
		    choppedEnd = True;
		}

		/*
		 * Now reset length so that it represents the number of bytes
		 * in the string.
		 */
		length = end - start;

		break;
	    }
	    }

	    /*
	     * Having truncated string recalculate extents to find origin
	     */
	    XmbTextPerCharExtents(font_set, string, length,
		ink_array, logical_array, length, &num_chars,
		NULL, &overall_logical);
	}
	/*
	 * We fit inside our cell, so just compute the x of the start of
	 * our string
	 */
	else
	{
	    switch (alignment)
	    {

	    case XmALIGNMENT_CENTER:
		x += maxwidth / 2 - overall_logical.width / 2;
		break;

	    case XmALIGNMENT_END:
		x += maxwidth - overall_logical.width;
		break;

	    case XmALIGNMENT_BEGINNING:
	    default:
		/*
		 * Leave x alone
		 */
		break;
	    }
	}

	/*
	 * Don't worry, XSetForeground is smart about avoiding unnecessary
	 * protocol requests.
	 */
	XSetForeground(XtDisplay(mw), gc, color);

	if (mw->matrix.show_arrows && choppedStart)
	{
	    XPoint points[ 3 ];
	    points[ 0 ].x = points[ 1 ].x = x + mw->matrix.font_width;
	    points[ 0 ].y = y + mw->matrix.font_y;
	    points[ 1 ].y = y + mw->matrix.font_y + mw->matrix.font_height;
	    points[ 2 ].x = x;
	    points[ 2 ].y = y + mw->matrix.font_y + mw->matrix.font_height / 2;

	    XFillPolygon(XtDisplay(mw), win, gc, points, 3,
			 Convex, CoordModeOrigin);

	    /* Offset the start point so as to not draw on the triangle */
	    x += FONT_WIDTH(mw);
	}

	if (mw->matrix.show_arrows && choppedEnd)
	{
	    XPoint points[ 3 ];
	    points[ 0 ].x = points[ 1 ].x = x + overall_logical.width;
	    points[ 0 ].y = y + mw->matrix.font_y;
	    points[ 1 ].y = y + mw->matrix.font_y + mw->matrix.font_height;
	    points[ 2 ].x = x + overall_logical.width + mw->matrix.font_width;
	    points[ 2 ].y = y + mw->matrix.font_y + mw->matrix.font_height / 2;

	    XFillPolygon(XtDisplay(mw), win, gc, points, 3,
			 Convex, CoordModeOrigin);
	}

	/*
	 * Adjust x for origin of string.
	 */
	x -= overall_logical.x;

#if 1
	clipht.height = y + font_height;
	clipht.width = x + maxwidth;
	clipht.x = 0;
	clipht.y = 0;

	if (doclip) {
		DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
			"XSetClipRectangles - x %d y %d w %d h %d\n",
			x, y, clipht.width, clipht.height));
		XSetClipRectangles(XtDisplay(mw), gc, 0, 0, &clipht, 1, Unsorted);
	}
#endif
	/*
	 * Now draw the string at x starting at char 'start' and of
	 * length 'length'
	 */
        /*
         * Scott MacHaffie, June 8, 2001
         * Fixes for multiple row heights:
         *
         * To correctly display right-aligned text, we need to do
         * the following:
         *
         * 1. shift y down to the bottom of the cell: y += maxht - font_height;
         * 2. call xbaeDrawString() again with start reset to 0 and length
         * set to: length = strlen(string) - length;
         * and maxht set to: maxht -= font_height;
         * Also, the code should bail out if maxht < font_height.
         * This will draw subsequent rows above the current row.
         *
         * To correctly display center-aligned text, we need to figure out
         * if maxht contains an even number of rows or an odd number of rows.
         *
         * If it contains an odd number of rows, then we draw as follows:
         * 1. The given string is drawn at the center row of the cell.
         * 2. Then we take the left part of the string (0 to start), and
         * draw it right aligned from original y to the current y.
         * 3. Then we take the right part of the string (length to strlen),
         * and draw it left aligned from the current y + font_height to
         * maxht.
         *
         * If the cell contains an even number of rows, then we
         * conceptually do the same thing: treat the current string and
         * row as null, then right-align the first half of the string
         * in the upper half of the cell and left-align the second half
         * of the string in the bottom half of the cell.  The middle point
         * of the string needs to be determined, pixel-wise.
         *
         * Given: my time constraints, the fact that we only need left-
         * aligned text to work, and the amount of work involved in
         * testing all of these options, I have not implemented these
         * other two alignments.
         *
         * See below for the multi-row left-aligned text.
         */
	XmbDrawString(XtDisplay(mw), win, font_set, gc, x, y, &string[start],
		      length);

	/*
	 * If bold is on, draw the string again offset by 1 pixel (overstrike)
	 */
	if (bold)
	    XmbDrawString(XtDisplay(mw), win, font_set, gc, x - 1, y,
			  &string[start], length);
	if (ink_array)
	    XtFree((char*)ink_array);
	if (logical_array)
	    XtFree((char*)logical_array);
        
        if (start + length < strlen(string) && alignment == XmALIGNMENT_BEGINNING) {
            /*
             * Scott MacHaffie, June 8, 2001
             * Fix for multiple row heights
             *
             * Only the first line of the string has been drawn.
             * Now we need to repeat the drawing, having shifted down one line.
             */
	    if (XbaeMultiLineCell(mw)) {
		xbaeDrawString(mw, win, gc,
			&string[start + length], strlen(&string[start + length]),
			x, y + font_height, maxlen, maxht - font_height,
			alignment, highlight, bold, rowLabel, colLabel, color);
	    }
        }
    }
    else
    {
	width = XTextWidth(font_struct, string, length);

	/*
	 * If the width of the string is greater than the width of this cell,
	 * we need to clip. We don't want to use the server to clip because
	 * it is slow, so we truncate characters if we exceed a cells pixel
	 * width.
	 */
	if (width > maxwidth)
	{
	    switch (alignment)
	    {

	    case XmALIGNMENT_CENTER:
	    {
		int startx = x;
		int endx = x + maxwidth - 1;
		int newendx;

		/*
		 * Figure out our x for the centered string.  Then loop
		 * and chop characters off the front until we are within
		 * the cell.
		 *
		 * Adjust x, the starting character and the length of the
		 * string for each char.
		 */
		x += maxwidth / 2 - width / 2;
		while (x < startx)
		{
		    int cw = charWidth(font_struct,
				       (unsigned char)string[start]);

		    x += cw;
		    width -= cw;
		    length--;
		    start++;
		    choppedStart = True;
		}

		/*
		 * Now figure out the end x of the string.  Then loop and chop
		 * characters off the end until we are within the cell.
		 */
		newendx = x + width - 1;
		while (newendx > endx && *(string + start))
		{
		    int cw = charWidth(font_struct,
				       (unsigned char)string[start]);

		    newendx -= cw;
		    width -= cw;
		    length--;
		    choppedEnd = True;
		}

		break;
	    }

	    case XmALIGNMENT_END:
	    {

		/*
		 * Figure out our x for the right justified string.
		 * Then loop and chop characters off the front until we fit.
		 * Adjust x for each char lopped off. Also adjust the starting
		 * character and length of the string for each char.
		 */
		x += maxwidth - width;
		while (width > maxwidth)
		{
		    int cw = charWidth(font_struct,
				       (unsigned char)string[start]);

		    width -= cw;
		    x += cw;
		    length--;
		    start++;
		    choppedStart = True;
		}
		break;
	    }

	    case XmALIGNMENT_BEGINNING:
	    default:
		/*
		 * Leave x alone, but chop characters off the end until we fit
		 */
		while (width > maxwidth && length > 0 )
		{
		    width -= charWidth(font_struct,
				       (unsigned char)string[length - 1]);
		    length--;
		    choppedEnd = True;
		}
		break;
	    }
	}

	/*
	 * We fit inside our cell, so just compute the x of the start of
	 * our string
	 */
	else
	{
	    switch (alignment)
	    {

	    case XmALIGNMENT_CENTER:
		x += maxwidth / 2 - width / 2;
		break;

	    case XmALIGNMENT_END:
		x += maxwidth - width;
		break;

	    case XmALIGNMENT_BEGINNING:
	    default:
		/*
		 * Leave x alone
		 */
		break;
	    }
	}

	/*
	 * Don't worry, XSetForeground is smart about avoiding unnecessary
	 * protocol requests.
	 */
	XSetForeground(XtDisplay(mw), gc, color);

	if (mw->matrix.show_arrows && choppedEnd)
	{
	    XPoint points[3];
	    points[0].x = points[1].x = x + width - mw->matrix.font_width;
	    points[0].y = y + mw->matrix.font_y;
	    points[1].y = y + mw->matrix.font_y + mw->matrix.font_height;
	    points[2].x = x + width;
	    points[2].y = y + mw->matrix.font_y + mw->matrix.font_height / 2;

	    XFillPolygon(XtDisplay(mw), win, gc, points, 3,
			 Convex, CoordModeOrigin);

	    /* Reduce the length to allow for our foreign character */
	    length--;
	}
	if (mw->matrix.show_arrows && choppedStart)
	{
	    XPoint points[3];
	    points[0].x = points[1].x = x + mw->matrix.font_width;
	    points[0].y = y + mw->matrix.font_y;
	    points[1].y = y + mw->matrix.font_y + mw->matrix.font_height;
	    points[2].x = x;
	    points[2].y = y + mw->matrix.font_y + mw->matrix.font_height / 2;

	    XFillPolygon(XtDisplay(mw), win, gc, points, 3,
			 Convex, CoordModeOrigin);

	    /* Offset the start point so as to not draw on the triangle */
	    x += mw->matrix.font_width;
	    start++;
	    length--;
	}

	/*
	 * Now draw the string at x starting at char 'start' and of length
	 * 'length'
	 */
#if 1
    clipht.width = maxwidth + x;
    clipht.height = font_height + y;
    clipht.x = 0;
    clipht.y = 0;

    if (doclip) {
	DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
		"XSetClipRectangles 2 - x %d y %d w %d h %d\n",
		x, y, clipht.width, clipht.height));
	XSetClipRectangles(XtDisplay(mw), gc, 0, 0, &clipht, 1, Unsorted);
    }
#endif
#ifdef NEED_WCHAR
	if (TWO_BYTE_FONT(mw))
	    XDrawString16(XtDisplay(mw), win, gc, x, y, &string[start],
			  length);
	else
#endif
	    XDrawString(XtDisplay(mw), win, gc, x, y, &string[start], length);

	/*
	 * If bold is on, draw the string again offset by 1 pixel (overstrike)
	 */
	if (bold)
#ifdef NEED_WCHAR
	    if (TWO_BYTE_FONT(mw))
		XDrawString16(XtDisplay(mw), win, gc, x - 1, y,
			      &string[start], length);
	    else
#endif
		XDrawString(XtDisplay(mw), win, gc, x - 1, y,
			    &string[start], length);
            if (start + length < strlen(string) && alignment == XmALIGNMENT_BEGINNING) {
                /*
                 * Scott MacHaffie, June 8, 2001
                 * Fix for multiple row heights
                 *
                 * Only the first line of the string has been drawn.
                 * Now we need to repeat the drawing, having shifted
                 * down one line.
                 * Please see my comments above about how to fix
                 * right- and center-aligned text.
                 */
		if (XbaeMultiLineCell(mw)) {
			xbaeDrawString(mw, win, gc,
				&string[start + length], strlen(&string[start + length]),
				x, y + font_height, maxlen, maxht - font_height,
				alignment, highlight, bold, rowLabel, colLabel, color);
		}
            }
    }

    if (doclip) {
	XSetClipMask(XtDisplay(mw), gc, None);
    }
}

void
xbaeComputeCellColors(XbaeMatrixWidget mw,
			int row,
			int column,
			Pixel *fg,
			Pixel *bg)
{
    Boolean alt = mw->matrix.alt_row_count ?
	(row / mw->matrix.alt_row_count) % 2 : False;

    /*
     * Compute the background and foreground colours of the cell
     */
    if (mw->matrix.selected_cells && mw->matrix.selected_cells[row][column]) {
	if (mw->matrix.reverse_select) {
	    if (mw->matrix.colors)
		*bg = mw->matrix.colors[row][column];
	    else
		*bg = mw->manager.foreground;
        }
	else
	    *bg = mw->matrix.selected_background;
    }
    else if (mw->matrix.cell_background &&
	     mw->matrix.cell_background[row][column] !=
	     mw->core.background_pixel)
	*bg = mw->matrix.cell_background[row][column];
    else
    {
	if (alt)
	    *bg = mw->matrix.odd_row_background;
	else
	    *bg = mw->matrix.even_row_background;
    }

    if (mw->matrix.selected_cells && mw->matrix.selected_cells[row][column]) {
	if (mw->matrix.reverse_select) {
	    if (mw->matrix.cell_background)
		*fg = mw->matrix.cell_background[row][column];
	    else
		*fg = mw->core.background_pixel;
        }
	else
	    *fg = mw->matrix.selected_foreground;
    }
    else if (mw->matrix.colors)
	*fg = mw->matrix.colors[row][column];
    else
	*fg = mw->manager.foreground;
}

void
xbaeComputeCellBg(XbaeMatrixWidget mw,
			int row,
			int column,
			Pixel *bg)
{
    *bg = mw->core.background_pixel;
}

void
xbaeDrawCell(XbaeMatrixWidget mw, int row, int column)
{
    Pixel bg, fg;
    String string;
    int x, y;

    if (mw->matrix.disable_redisplay || mw->matrix.rows == 0 ||
	mw->matrix.columns == 0)
	return;

/*    DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw, "xbaeDrawCell: %d %d\n", row, column)); */

    /*
     * Convert the row/column to the coordinates relative to the correct
     * window
     */
    xbaeRowColToXY(mw, row, column, &x, &y);

    xbaeComputeCellColors(mw, row, column, &fg, &bg);

    if (mw->matrix.cell_widgets && mw->matrix.cell_widgets[row][column])
	xbaeDrawCellWidget(mw, row, column, x, y,
			   mw->matrix.cell_widgets[row][column], bg, fg);
    else
	if (!mw->matrix.draw_cell_callback)
	{
	    if (row < mw->matrix.rows && column < mw->matrix.columns)
	    {
		string = mw->matrix.cells ?
		    mw->matrix.cells[row][column] : "";
		xbaeDrawCellString(mw, row, column, x, y, string, bg, fg);
	    }
	}
	else
	{
	    Pixmap pixmap;
	    Pixmap mask;
	    XbaeCellType type;
	    int width, height;
	    int depth;

	    if (row < mw->matrix.rows && column < mw->matrix.columns)
	    {
		type = xbaeGetDrawCellValue(mw, row, column, &string, &pixmap,
					    &mask, &width, &height, &bg, &fg,
					    &depth);
		if (type == XbaeString)
		    xbaeDrawCellString(mw, row, column, x, y, string, bg, fg);
		else if (type == XbaePixmap)
		    xbaeDrawCellPixmap(mw, row, column, x, y, pixmap, mask,
				       width, height, bg, fg, depth);
	    }
	}
}

XbaeCellType
xbaeGetDrawCellValue(XbaeMatrixWidget mw,
			int row,
			int column,
			String *string,
			Pixmap *pixmap,
			Pixmap *mask,
			int *width,
			int *height,
			Pixel *bg,
			Pixel  *fg,
			int *depth)
{
    XbaeMatrixDrawCellCallbackStruct call_data;

    call_data.reason = XbaeDrawCellReason;
    call_data.event = (XEvent *)NULL;
    call_data.row = row;
    call_data.column = column;
    call_data.width = COLUMN_WIDTH(mw, column) - TEXT_WIDTH_OFFSET(mw) * 2;
    call_data.height = SOME_ROW_HEIGHT(mw, row) - TEXT_HEIGHT_OFFSET(mw) * 2;
    call_data.type = XbaeString;
    call_data.string = "";
    call_data.pixmap = (Pixmap)NULL;
    call_data.mask = (Pixmap)NULL;
    call_data.foreground = *fg;
    call_data.background = *bg;
    call_data.depth = 0;

    XtCallCallbackList((Widget)mw, mw->matrix.draw_cell_callback,
		       (XtPointer) &call_data);

    *pixmap = call_data.pixmap;
    *mask = call_data.mask;
    *string = call_data.string ? call_data.string : ""; /* Handle NULLs */

    if (mw->matrix.reverse_select && mw->matrix.selected_cells &&
	mw->matrix.selected_cells[row][column])
    {
	/*
	 * if colours were set by the draw cell callback, handle reverse
	 * selection
	 */
	if (*bg != call_data.background)
	{
	    if (*fg != call_data.foreground)
		*bg = call_data.foreground;
	    *fg = call_data.background;
	}
	else if (*fg != call_data.foreground)
	    *bg = call_data.foreground;
    }
    else
    {
	*fg = call_data.foreground;
	*bg = call_data.background;
    }
    *width = call_data.width;
    *height = call_data.height;
    *depth = call_data.depth;

    if (call_data.type == XbaePixmap)
    {
	if (*mask == XmUNSPECIFIED_PIXMAP || *mask == BadPixmap)
	    call_data.mask = 0;

	if (*pixmap == XmUNSPECIFIED_PIXMAP || *pixmap == BadPixmap)
	{
	    XtAppWarningMsg(
		XtWidgetToApplicationContext((Widget)mw),
		"drawCellCallback", "Pixmap", "XbaeMatrix",
		"XbaeMatrix: Bad pixmap passed from drawCellCallback",
		NULL, 0);
	    call_data.type = XbaeString;
	    *string = "";
	}
	else if (!*depth)
	{
	     /*
	      * If we know the depth, width and height don't do a round
	      * trip to find the
	      * geometry
	      */
	    Window root_return;
	    int x_return, y_return;
	    unsigned int width_return, height_return;
	    unsigned int border_width_return;
	    unsigned int depth_return;

	    if (XGetGeometry(XtDisplay(mw), *pixmap, &root_return,
			     &x_return, &y_return, &width_return,
			     &height_return, &border_width_return,
			     &depth_return))
	    {
		*width = width_return;
		*height = height_return;
		*depth = depth_return;
	    }
	}
    }
    return (call_data.type);
}

/*
 * Draw the column label for the specified column.  Handles labels in
 * fixed and non-fixed columns.
 */
void
xbaeDrawColumnLabel(XbaeMatrixWidget mw, int column, Boolean pressed)
{
    String label;
    int labelX, labelY;
    int buttonX;
    int i;
    GC gc;
    Window win = XtWindow(mw);
    Boolean clipped = (column >= (int)mw->matrix.fixed_columns &&
		       column < TRAILING_HORIZ_ORIGIN(mw));

    Boolean button = mw->matrix.button_labels ||
	(mw->matrix.column_button_labels &&
	 mw->matrix.column_button_labels[column]);

	DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
		"xbaeDrawColumnLabel(%d)\n", column));

    if (mw->matrix.column_labels[column][0] == '\0' && !button)
	return;

    /*
     * If the column label is in a fixed column, we don't need to account
     * for the horiz_origin
     */
    if (column < (int)mw->matrix.fixed_columns)
    {
	labelX = COLUMN_LABEL_OFFSET(mw) + COLUMN_POSITION(mw, column) +
	    TEXT_X_OFFSET(mw);
	buttonX = COLUMN_LABEL_OFFSET(mw) + COLUMN_POSITION(mw, column);
    }
    else if (column >= TRAILING_HORIZ_ORIGIN(mw))
    {
	labelX = TRAILING_FIXED_COLUMN_LABEL_OFFSET(mw) +
	    COLUMN_POSITION(mw, column) -
	    COLUMN_POSITION(mw, TRAILING_HORIZ_ORIGIN(mw)) +
	    TEXT_X_OFFSET(mw);
	buttonX = TRAILING_FIXED_COLUMN_LABEL_OFFSET(mw) +
	    COLUMN_POSITION(mw, column) -
	    COLUMN_POSITION(mw, TRAILING_HORIZ_ORIGIN(mw));
    }
    else
    {
	labelX = COLUMN_LABEL_OFFSET(mw) +
	    (COLUMN_POSITION(mw, column) - HORIZ_ORIGIN(mw)) +
	    TEXT_X_OFFSET(mw);
	buttonX = COLUMN_LABEL_OFFSET(mw) + (COLUMN_POSITION(mw, column) -
					     HORIZ_ORIGIN(mw));
    }

    /*
     * Set our y to the baseline of the first line in this column
     */
    labelY = -mw->matrix.label_font_y +
	mw->matrix.cell_shadow_thickness +
	mw->matrix.cell_highlight_thickness +
	mw->matrix.cell_margin_height +
	mw->matrix.text_shadow_thickness +
	(mw->matrix.column_label_maxlines -
	 mw->matrix.column_label_lines[column].lines) * LABEL_HEIGHT(mw) +
	HORIZ_SB_OFFSET(mw);

    if (clipped)
	gc = mw->matrix.label_clip_gc;
    else
	gc = mw->matrix.label_gc;

    if (button)
    {
	XSetForeground(XtDisplay(mw), gc, mw->matrix.button_label_background);
	XFillRectangle(XtDisplay(mw), win, gc, buttonX, HORIZ_SB_OFFSET(mw),
		       COLUMN_WIDTH(mw, column), COLUMN_LABEL_HEIGHT(mw));
    }

    XSetForeground(XtDisplay(mw), gc, mw->matrix.column_label_color);
    XSetBackground(XtDisplay(mw), gc, mw->matrix.button_label_background);

    label = mw->matrix.column_labels[column];

    if (label[0] != '\0')
	for (i = 0; i < mw->matrix.column_label_lines[column].lines; i++)
	{
	    xbaeDrawString(mw, XtWindow(mw), gc, label,
			   mw->matrix.column_label_lines[column].lengths[i],
			   labelX, labelY, mw->matrix.column_widths[column],
                         -1,
			   mw->matrix.column_label_alignments ?
			   mw->matrix.column_label_alignments[column] :
                         XmALIGNMENT_BEGINNING, False,
			   mw->matrix.bold_labels, False, True,
			   mw->matrix.column_label_color);

	    labelY += LABEL_HEIGHT(mw);
	    label += mw->matrix.column_label_lines[column].lengths[i] + 1;
	}
    if (button)
	xbaeDrawCellShadow(mw, XtWindow(mw), -1, column,
			   buttonX, HORIZ_SB_OFFSET(mw),
			   COLUMN_WIDTH(mw, column),
			   COLUMN_LABEL_HEIGHT(mw), True, clipped, pressed);
}

/*
 * Draw the row label for the specified row. Handles labels in fixed and
 * non-fixed rows.
 */
void
xbaeDrawRowLabel(XbaeMatrixWidget mw, int row, Boolean pressed)
{
    int		y;
    int		 i;
    GC gc;
    Window win = XtWindow(mw);
    Boolean clipped = (row >= (int)mw->matrix.fixed_rows &&
		       row < TRAILING_VERT_ORIGIN(mw));

    Boolean button = mw->matrix.button_labels ||
	(mw->matrix.row_button_labels && mw->matrix.row_button_labels[row]);

    if (mw->matrix.row_labels[row][0] == '\0' && !button) {
	DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
		"xbaeDrawRowLabel(%d) return\n", row));

	return;
    }

	DEBUGOUT(_XbaeDebug(__FILE__, (Widget)mw,
		"xbaeDrawRowLabel(%d) clipped %d\n",
		row, clipped));

    /*
     * SGO: Made some changes. draw the label always. also the handling of
     * row_heights changed. Its now always used, but if row_heights_used
     * is set, the user has to manage size adaptions and so on. Otherwise,
     * the default ROW_HEIGHT is used for each row.
     *
     * If the row label is in a fixed row we don't need to account
     * for the vert_origin
     */
    if (row < (int)mw->matrix.fixed_rows) {
	if (mw->matrix.row_heights_used) {
	    y = ROW_LABEL_OFFSET(mw) + TEXT_Y_OFFSET(mw);
	    for (i=0; i<row; i++)
		y += mw->matrix.row_heights[i];
	} else {
	    y = ROW_LABEL_OFFSET(mw) + ROW_HEIGHT(mw) * row + TEXT_Y_OFFSET(mw);
	}
    } else if (row >= TRAILING_VERT_ORIGIN(mw)) {
	y = TRAILING_FIXED_ROW_LABEL_OFFSET(mw) +
	    TEXT_Y_OFFSET(mw);
	for (i=TRAILING_VERT_ORIGIN(mw); i<row; i++)
		y += mw->matrix.row_heights[i];
    } else {
	y = xbaeRowToY(mw, row) + ROW_LABEL_OFFSET(mw) + LABEL_Y_OFFSET(mw);

	for (i=0; i<mw->matrix.fixed_rows; i++)
		y += mw->matrix.row_heights[i];
    }

    if (clipped)
	gc = mw->matrix.label_clip_gc;
    else
	gc = mw->matrix.label_gc;

    if (button) {
	XSetForeground(XtDisplay(mw), gc, mw->matrix.button_label_background);
	XFillRectangle(XtDisplay(mw), win, gc, VERT_SB_OFFSET(mw),
		       y - TEXT_Y_OFFSET(mw), ROW_LABEL_WIDTH(mw),
		       ROW_HEIGHT(mw));
    }

    XSetForeground(XtDisplay(mw), gc, mw->matrix.row_label_color);
    XSetBackground(XtDisplay(mw), gc, mw->matrix.button_label_background);

    if (mw->matrix.row_labels[row][0] != '\0') {
	xbaeDrawString(mw, win, gc,
		mw->matrix.row_labels[row],
		strlen(mw->matrix.row_labels[row]),
		TEXT_X_OFFSET(mw) + VERT_SB_OFFSET(mw), y,
		mw->matrix.row_label_width,	/* width clip */
		-1,				/* height clip */
		mw->matrix.row_label_alignment, False,
		mw->matrix.bold_labels, True, False,
		mw->matrix.row_label_color);
    }

    if (button)
	xbaeDrawCellShadow(mw, win, row, -1, VERT_SB_OFFSET(mw),
			   y - TEXT_Y_OFFSET(mw), ROW_LABEL_WIDTH(mw),
			   SOME_ROW_HEIGHT(mw, row), True, clipped, pressed);
}

package xmext

author	Igor.Soloviev@cern.ch
manager	Igor.Soloviev@cern.ch Andrei.Kazarov@cern.ch

use OnlinePolicy

#############################################################################################################

public

# applications using xmext library to be linked with $(xmext_libs) macro

macro		xmext_libs			"-lxmext "

macro_append	xmext_libs			""			\
		sunos				" -ljpeg"		\
		linux				" -ljpeg"		\
		osx				" -ljpeg"

#############################################################################################################

private

library		xmext $(lib_opts) 		../HTTP/*.c		\
						../XmHTML/XmHTML.c	\
						../XmHTML/Balloon.c	\
						../XmHTML/XmImage.c	\
						../XmHTML/fonts.c	\
						../XmHTML/callbacks.c	\
						../XmHTML/events.c	\
						../XmHTML/frames.c	\
						../XmHTML/forms.c	\
						../XmHTML/StringUtil.c	\
						../XmHTML/parse.c	\
						../XmHTML/format.c	\
						../XmHTML/layout.c	\
						../XmHTML/motif.c	\
						../XmHTML/paint.c	\
						../XmHTML/private.c	\
						../XmHTML/htmlpublic.c	\
						../XmHTML/colors.c	\
						../XmHTML/images.c	\
						../XmHTML/readBitmap.c	\
						../XmHTML/readFLG.c	\
						../XmHTML/readGIF.c	\
						../XmHTML/readGIFplc.c	\
						../XmHTML/readXPM.c	\
						../XmHTML/readJPEG.c	\
						../XmHTML/readJPEGplc.c	\
						../XmHTML/readPNG.c	\
						../XmHTML/map.c		\
						../XmHTML/XCC.c		\
						../XmHTML/quantize.c	\
						../XmHTML/LZWStream.c	\
						../XmHTML/plc.c		\
						../XmHTML/error.c	\
						../XmHTML/stack.c	\
						../XmHTML/output.c	\
						../XmHTML/debug.c	\
						../XmHTML/warnings.c	\
						../XmHTML/strings.c	\
						../Tree/Tree.c		\
						../Xbae/*.c		\
						../ListTree/ListTree.c	\
						../XgTabs/*.c

macro		xmext_shlibflags		""	\
		osx				"-ljpeg $(X_linkopts) -single_module"

macro_append    lib_xmext_pp_cflags		"-I../Tree -I../XmHTML $(X_includes) -DVERSION=1104 	\
					 	-Dproduction -DNDEBUG -D_GNU_SOURCE -D_BSD_SOURCE "
		
macro_append	lib_xmext_pp_cflags		""							\
		sunos				" -D_POSIX_SOURCE -DHAVE_LIBJPEG -D__EXTENSIONS__ "	\
		linux				" -DHAVE_LIBJPEG " \
		osx				" -DHAVE_LIBJPEG "

application	xbae-matrix -no_prototypes	../examples/Xbae/xbae-matrix.c
macro		xbae-matrixlinkopts		"$(xmext_libs) $(X_linkopts) $(socket_libs) $(thread_libs)"
macro		xbae-matrix_dependencies	xmext
macro		app_xbae-matrix_pp_cflags	$(X_includes)

application	xmhtml-http -no_prototypes	../examples/XmHTML/xmhtml-http.c
macro		xmhtml-httplinkopts		"$(xmext_libs) $(X_linkopts) $(socket_libs) $(thread_libs)"
macro		xmhtml-http_dependencies	xmext
macro		app_xmhtml-http_pp_cflags	$(X_includes)

application	xmtree -no_prototypes		../examples/Tree/xmtree.c
macro		xmtreelinkopts			"$(xmext_libs) $(X_linkopts) $(socket_libs) $(thread_libs)"
macro		xmtree_dependencies		xmext
macro		app_xmtree_pp_cflags		$(X_includes)

application	list-tree -no_prototypes	../examples/ListTree/list-tree.c
macro		list-treelinkopts		"$(xmext_libs) $(X_linkopts) $(socket_libs) $(thread_libs)"
macro		app_list-tree_pp_cflags		$(X_includes)
macro		list-tree_dependencies		xmext

application	xg-tabs -no_prototypes		../examples/XgTabs/xg-tabs.c
macro		xg-tabslinkopts			"$(xmext_libs) $(X_linkopts) $(socket_libs) $(thread_libs)"
macro		xg-tabs_dependencies		xmext
macro		app_xg-tabs_pp_cflags		$(X_includes)

macro		my_libs				libxmext.so						\
		lynxos				libxmext.a						\
		osx				libxmext.dylib				

apply_pattern	install_libs			files=$(my_libs)

apply_pattern	install_headers 		src_dir="../Xm"						\
						target_dir="../Xm"					\
						files="*.h"
						
#############################################################################################################

# install Xbae documentation

apply_pattern	install_docs name="xbae-html"	target_dir="xbae/html"					\
						src_dir="../Xbae/doc/html"				\
						files="*.html"

apply_pattern	install_docs name="xbae-html-i"	target_dir="xbae/html/images"				\
						src_dir="../Xbae/doc/html/images"			\
						files="*.jpg *.png"
	
apply_pattern	install_docs name="xbae-txt"	target_dir="xbae"					\
						src_dir="../Xbae/doc"					\
						files="AUTHORS COPYING ChangeLog NEWS README"

apply_pattern	install_docs name="xbae-man"	target_dir="xbae/man/man3"				\
						src_dir="../Xbae/doc/man"				\
						files="*.3"

						
						

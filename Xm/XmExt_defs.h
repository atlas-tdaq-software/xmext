#ifndef __XmExt_DEFS
#define __XmExt_DEFS

	/**	
	***  XmExtExport keyword is used to build XmExt
	***  library as Windows NT DLL
	**/
	
#ifdef _MSC_VER
#  ifdef _XmExtBuildDll_
#    define XmExtExport __declspec(dllexport)
#  else
#    define XmExtExport __declspec(dllimport)
#  endif
#else
#  define XmExtExport
#endif

#endif
